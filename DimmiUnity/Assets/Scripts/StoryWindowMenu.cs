﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StoryWindowMenu : MonoBehaviour {

    public GameObject selectonWindow;
    public GameObject blocker;
    public Image photo;
    public float alfa;
    public Toggle loadfromSave;
    public List<string> descriptions, names;
    public int selectedCharacter;
    public string selectedName;
    public Text nameText, descText, storylineText, continueText;
    public Transform elliProg, miskaProg, jariProg, aslaProg;
    public Color b, g, w;
    public MenuScript ms;

    void Start ()
    {
        //set up the variables
        ms = GetComponent<MenuScript>();
        names = new List<string>() {"Elli","Miska","Asla","Jari"};
        descriptions = new List<string>() {"Ikä: 18\nOpiskelija","","",""};
        selectonWindow = GameObject.Find("Story");
        blocker = GameObject.Find("UIBlocker");
        loadfromSave = GameObject.Find("ValintaToggle").GetComponent<Toggle>();
        elliProg = GameObject.Find("ElliProgressBar").transform;
        miskaProg = GameObject.Find("MiskaProgressBar").transform;
        jariProg = GameObject.Find("JariProgressBar").transform;
        aslaProg = GameObject.Find("AslaProgressBar").transform;
        LoadProgress();
    }
	void LoadProgress()
    {
        //load progress bars
        List<Transform> t = new List<Transform>() { };
        foreach(Transform child in elliProg)
        {
            child.GetComponent<Image>().color = b;
            t.Add(child);
        }
        for(int i = 0; i < PlayerPrefs.GetInt("ElliProgress"); i++)
        {
            t[i].GetComponent<Image>().color = g;
        }

        try
        {
            t[PlayerPrefs.GetInt("ElliProgress")].GetComponent<Image>().color = w;
        }
        catch
        {
            print("Over 9, Story Completed");
        }
        t.Clear();

        foreach (Transform child in miskaProg)
        {
            child.GetComponent<Image>().color = b;
            t.Add(child);
        }   
        for (int i = 0; i < PlayerPrefs.GetInt("MiskaProgress"); i++)
        {
            t[i].GetComponent<Image>().color = g;
        }
        t[PlayerPrefs.GetInt("MiskaProgress")].GetComponent<Image>().color = w;
        t.Clear();

        foreach (Transform child in jariProg)
        {
            child.GetComponent<Image>().color = b;
            t.Add(child);
        }
        for (int i = 0; i < PlayerPrefs.GetInt("JariProgress"); i++)
        {
            t[i].GetComponent<Image>().color = g;
        }
        t[PlayerPrefs.GetInt("JariProgress")].GetComponent<Image>().color = w;
        t.Clear();

        foreach (Transform child in aslaProg)
        {
            child.GetComponent<Image>().color = b;
            t.Add(child);
        }
        for (int i = 0; i < PlayerPrefs.GetInt("AslaProgress"); i++)
        {
            t[i].GetComponent<Image>().color = g;
        }
        t[PlayerPrefs.GetInt("AslaProgress")].GetComponent<Image>().color = w;
        t.Clear();
    }

	void Update ()
    {
        try
        {
            selectonWindow.GetComponent<CanvasGroup>().alpha = Mathf.Lerp(selectonWindow.GetComponent<CanvasGroup>().alpha, alfa, 5f * Time.deltaTime);
        }
        catch
        {
            print("Window not active");
        }
	}
    public void LoadStatuss(string n)
    {
        //opens up the specific story part selection
        int prog = PlayerPrefs.GetInt(n + "Completed");
        Debug.Log(PlayerPrefs.GetInt("Elli" + "Completed") + " / ? ");
        blocker.SetActive(PlayerPrefs.GetInt(n + "Completed") == 1 ? false : true);
        
    }

    public void CloseWindow()
    {
        //closes the window
        selectonWindow.GetComponent<CanvasGroup>().interactable = false;
        selectonWindow.GetComponent<CanvasGroup>().blocksRaycasts = false;
        alfa = 0f;
    }
    public void ChangeLoadToggle()
    {
        //checks if you want to load from selection or not
        int i = loadfromSave.enabled ? 1 : 0;
        PlayerPrefs.SetInt("LoadGame", i);
    }
    public void ChangeName(string n)
    {
        //changes the level
        selectedName = n;
    }
    public void LoadTargetLevel(int i)
    {
        //load the specific level
        string level2Load = selectedName + i;
        PlayerPrefs.SetString("SelectedStory", level2Load);
        PlayerPrefs.SetInt("CustomStory", 0);
        PlayerPrefs.SetInt("PartOfProgress", 1);
        ms.GetAndLoad();
    }
    public void LoadLevel(string s)
    {
        //loads the level by progress
        int x = PlayerPrefs.GetInt(s + "Progress") == 0 ? 1 : PlayerPrefs.GetInt(s + "Progress") + 1;
        x = x > 9 ? 9 : x;
        string level2Load = s + x;
        PlayerPrefs.SetString("SelectedStory", level2Load);
        PlayerPrefs.SetInt("PartOfProgress", 1); 
        PlayerPrefs.SetInt("CustomStory", 0);
        ms.GetAndLoad();
    }
    public void ContinueGame(string name)
    {
        //continue the game from the chapter you played last time
        string save2load = "";
        save2load = name + PlayerPrefs.GetInt(name + "Progress");
        PlayerPrefs.SetString("SelectedStory", save2load);
    }
    public void StartOver()
    {
        //deletes your progress
        PlayerPrefs.SetInt(names[selectedCharacter] + "Progress", 0);
        PlayerPrefs.SetString("SelectedStory", selectedName + 1);
        PlayerPrefs.SetInt("CustomStory", 0);
        PlayerPrefs.SetInt("PartOfProgress", 1);
        ms.GetAndLoad();

    }
}
