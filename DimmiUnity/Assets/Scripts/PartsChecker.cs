﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartsChecker : MonoBehaviour {

    public List<Transform> t;
	// Use this for initialization
	void Start ()
    {
        //checks the parts
        foreach (Transform child in transform.GetComponentsInChildren<Transform>())
        {
            if (child == transform)
            {
                Debug.Log("err - parent object");
            }
            if (child != transform)
            {
                t.Add(child);
                Debug.Log(t.Count + " " + child.name);
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
