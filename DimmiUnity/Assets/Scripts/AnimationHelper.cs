﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationHelper : MonoBehaviour
{
    public int eyeCounter;
    public int eyeCounter2;
    private int tempEye;
    private int tempEye2;
    public int mouthCounter;
    private int tempMouth;

    private CharacterSetup csetup;


	// Use this for initialization
	void Start ()
    {
        csetup = GetComponent<CharacterSetup>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        //updates the eyes and mouths according to calls from animations
        if (tempEye2 != eyeCounter2)
        {
            try
            {
                csetup.Eyenimation2(eyeCounter2);
                tempEye2 = eyeCounter2;
            }
            catch
            {
                print("cant animate eyes");
            }
        }
        if (tempEye != eyeCounter)
        {
            try
            {
                csetup.Eyenimation(eyeCounter);
                tempEye = eyeCounter;
            }
            catch
            {
                print("cant animate eyes");
            }
        }
        if (tempMouth != mouthCounter)
        {
            try
            {
                csetup.Mouthimation(mouthCounter);
                tempMouth = mouthCounter;
            }
            catch
            {
                print("cant animate mouth");
            }
        }       
	}
}
