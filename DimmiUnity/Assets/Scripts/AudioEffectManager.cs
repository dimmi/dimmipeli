﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioEffectManager : MonoBehaviour {

    private AudioSource auds;
    public AudioClip[] clips;

	// Use this for initialization
	void Start ()
    {
        auds = GetComponent<AudioSource>();
	}
	
    public void PlayClip(int i)
    {
        //plays the selected audioclip from the array
        auds.PlayOneShot(clips[i]);
    }
    public void PlayClip(AudioClip ac)
    {
        //plays the selected audioclip
        auds.PlayOneShot(ac);
    }
}
