﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


[RequireComponent(typeof(AudioSource))]
public class GameScript : MonoBehaviour {

    public bool sounds;
    public GameObject malePrefab, femalePrefab;
    public JsonSaveLoad jsl;
    public Tooltip tt;
    public string currentBrach; //a, b, c . for example a12 is a 12th scene from the a branch. 
    public string storyName;
    public DialogueSystem ds;
    public PhoneScript phones;
    public DebugScript bugsy;
    public FadeInOut fio;
    public string[] scenarioList;
    public int[] scenarioNumber;
    public string[] scenarioSplit;

    public ScenarioClass curScene;
    public SpriteLibrary slib;
    public SpriteRenderer backgroundImage;
    public Sprite[] allBackgrounds;
    public List<GameObject> charactersInScene;//spawned characters

    public List<ScenarioClass> scenes;
    public List<string> characterNameList;//all character names for reading from JSON purposes
    public List<Character> allCharacter;//creates characters from namelist
    public string[] animations;
    public GameObject overlayCS; //overlay cutscene image
    public Image cutsceneImg;
    public bool lerpingcutscene;
    public GameObject endScreen;
    public Text dt; //debug text
    public CharacterLista characterList;
    public ScenarioClass su = new ScenarioClass(); //setup for savefile
    public ScenarioClass fad = new ScenarioClass(); //fade for savefile
    public AudioSource auds, ambauds;
    public AudioClip[] audioClips;
    public AudioManager audmanager;
    public Color fadeGray;
    public float[] zspawn;
    public float[] yspawn;
    public float[] lScale;
    public int[] laye;
    public GameObject optionWindow;
    public string nextStory;

    void Awake()
    {
        optionWindow = GameObject.Find("ReturnWindow");
        optionWindow.SetActive(false);
        auds = GetComponent<AudioSource>();
        slib = GetComponent<SpriteLibrary>();
        phones = GetComponent<PhoneScript>();
        audmanager = GameObject.Find("AmbientBlock").GetComponent<AudioManager>();
        int i = PlayerPrefs.GetInt("Sounds");
        if(i == 0)
        {
            sounds = false;
        }
        if(i == 1)
        {
            sounds = true;
        }
    }
    public void PlaySound(int i)
    {
        auds.PlayOneShot(audioClips[i]);
    }
    void Start ()
    {
        //sets up the debug text for use.
        if(Application.loadedLevelName != "GameScene")
        {
            dt = GameObject.Find("DebugText").GetComponent<Text>();
        }
        else
        {
            dt = GameObject.Find("jslBug").GetComponent<Text>();
        }
        allCharacter = new List<Character>();
        scenes = new List<ScenarioClass>();       
        jsl = GetComponent<JsonSaveLoad>();
        dt.text = "jsl.Loadcharray";
        characterList = jsl.GetResourceCharacterList();
        characterNameList = characterList.characters;
        endScreen = GameObject.Find("EndScreen");
        dt.text = "endscreen found";
        endScreen.SetActive(false);
        dt.text = "Start jsl char get";
        foreach(string s in characterNameList)
        {
            Character c = new Character();
            c = jsl.GetResourceCharacter(s);
            allCharacter.Add(c);
        }
        
        dt.text = "Finished jsl char get";
        cutsceneImg = GameObject.Find("CutsceneImage").GetComponent<Image>();
        overlayCS = GameObject.Find("CutsceneCanvas");
        overlayCS.SetActive(false);
        backgroundImage = GameObject.Find("BackgroundImage").GetComponent<SpriteRenderer>(); 
        ds = GetComponent<DialogueSystem>();
        tt = GetComponent<Tooltip>();
        bugsy = GetComponent<DebugScript>();
        fio = GameObject.Find("FadeInOut").GetComponent<FadeInOut>();
        print(PlayerPrefs.GetString("SelectedStory"));
        print(PlayerPrefs.GetInt("PartOfProgress"));
        storyName = PlayerPrefs.GetString("SelectedStory");
        print(storyName);
        dt.text = "Start loaded.";
        allBackgrounds = slib.backgroundSprites;
        StartCoroutine(StartGame());
       
	}
    public void SetSaveScenario(int i)
    {
        //auto saves.
        if (i == 0)
        {
            su.thisScenario = "SaveSetup";
            su.nextScenario = "SaveFade";
            su.scenarioType = 4;
            su.lookingRight = curScene.lookingRight;
            su.backgroundCounter = curScene.backgroundCounter;
            su.characterToSpawn = curScene.characterToSpawn;
            su.startPositions = curScene.startPositions;
            su.waitTime = curScene.waitTime;
            su.fadeIn = curScene.fadeIn;     
        }
        if (i == 1)
        {
            fad.thisScenario = "SaveFade";
            fad.fadeIn = true;
            fad.nextScenario = curScene.thisScenario;

            if(!jsl.DirectoryExists(storyName))
            {
                jsl.CreateDirectory(storyName);
            }

            jsl.SaveSaveFile(su, storyName);
            jsl.SaveSaveFile(fad, storyName);
        }
    }
   
    public void NextScene()
    {
        //gets the next scene info and calls the load function SelectNextScene
        dt.text = "next scene";
        if(curScene.thisScenario == "SaveSetup")
        {
            curScene = jsl.LoadSaveFile(storyName, curScene.nextScenario);
        }
        else
        {
            if(PlayerPrefs.GetInt("CustomStory") == 0)
            {
                curScene = jsl.GetResourceScenario(storyName, curScene.nextScenario);
            }
            if(PlayerPrefs.GetInt("CustomStory") == 1)
            {
                curScene = jsl.GetScenario(storyName, curScene.nextScenario);
            }
        }
        StartCoroutine(SelectNextScene(curScene.thisScenario, curScene.scenarioType));
        bugsy.DEB("Next Scene");
    }
    void ShowCutSceneImage(int i)
    {
        //loads a cutsceneimage
    }
    IEnumerator SelectNextScene(string i2, int i)//i = scenetype, i2 = scenenumber
    {
        //selects how the next scene is shown depending on the scene type.
        if (i != 1 && i != 2 && i!= 3 && i != 6)
        {
            ds.SetChatActive(false);
        }
        if(i != 13)
        {
            phones.ClosePhone();
        }
        Debug.Log("Scenetype: " + i);
        switch (i)
        {
            
            case 0: //fadeinout
                dt.text = "Fade started.";
                Debug.Log("Fade Started");
                if (curScene.fadeIn)
                {
                    fio.FadeIn();
                    yield return new WaitForSeconds(curScene.waitTime);
                }
                if(!curScene.fadeIn)
                {
                    fio.FadeOut();
                    yield return new WaitForSeconds(curScene.waitTime);
                }
                dt.text = "Fade ended.";
                Debug.Log("Fade Ended");
                NextScene();
                break;

            case 1: //Dialogue
                bool teller = curScene.isTeller;
                string diag = curScene.dialogue;
                string sname = curScene.characterName;

                StartCoroutine(StartDialogue(teller, sname, diag));
                break;

            case 2: //Selection
                StartCoroutine(StartSelectionDialogue(curScene.isTeller, curScene.characterName, curScene.dialogue, curScene.selectionOption1, curScene.selectionOption2));
                PlayerPrefs.SetString(storyName + "Save", curScene.thisScenario);
                SetSaveScenario(1);
                break;

            case 3: //Dialogue + Animation
                StartCoroutine(StartDialogue(curScene.isTeller, curScene.characterName, curScene.dialogue));
                for(int ar = 0; ar < curScene.charactersInScene.Length; ar++)
                {
                    if(curScene.isAnimated[ar])
                    {
                        StartCoroutine(StartAnimation(curScene.charactersInScene[ar], curScene.animationId[ar], curScene.secondaryWaitTime[ar], curScene.loopAnimation[ar], curScene.isAnimated[ar]));
                    }                   
                }              
                break;

            case 4: //setup
                SetupScene();
                yield return new WaitForSeconds(1.0f);
                yield return new WaitForSeconds(curScene.waitTime);
                

                SetSaveScenario(0);
                NextScene();
                break;

            case 5: //move
                for(int cm = 0; cm < curScene.charactersInScene.Length; cm++)
                {
                    if(curScene.moveThis[cm])
                    {
                        StartCoroutine(MoveCharacter(curScene.charactersInScene[cm], curScene.moveTo[cm], curScene.moveSpeed, curScene.lookingRight[cm], curScene.secondaryWaitTime[cm], curScene.lookingRight[cm]));
                    }
                }
                yield return new WaitForSeconds(curScene.waitTime);
                NextScene();
                break;

            case 6: //playsound
                audmanager.PlayMusicAmbient(curScene.audioClip);
                yield return new WaitForSeconds(curScene.waitTime);
                NextScene();
                break;

            case 7://overlay cutscene
                cutsceneImg.sprite = slib.cutSceneImages[curScene.selectedCutscene];
                overlayCS.SetActive(true);
                yield return new WaitForSeconds(curScene.waitTime);
                overlayCS.SetActive(false);
                NextScene();
                break;

            case 8://change clothing
                GameObject characterToChange = new GameObject();
                for(int te = 0; te < allCharacter.Count; te++)
                {
                    if(allCharacter[te].name == curScene.characterName)
                    {
                        characterToChange = GameObject.Find(curScene.characterName);
                    }
                }
                characterToChange.GetComponent<CharacterSetup>().ChangeClothes(curScene.clotheid, curScene.subid);
                yield return new WaitForSeconds(curScene.waitTime);
                NextScene();
                break;

            case 9://end game
                endScreen.SetActive(true);
                int c = 0;

                if(PlayerPrefs.GetInt("PartOfProgress") == 1)
                {                   
                    c = int.Parse(storyName.Substring(storyName.Length - 1, 1));
                    string x = storyName.Substring(0, storyName.Length - 1);
                    nextStory = "" + x + (c + 1);
                    print(nextStory);
                    PlayerPrefs.SetString("SelectedStory", nextStory);
                    PlayerPrefs.SetInt(x + "Progress", c);
                    yield return new WaitForSeconds(curScene.waitTime);
                    fio.FadeOut();
                    
                    if (c == 9)
                    {
                        PlayerPrefs.SetInt(x + "Completed", 1);
                        Debug.Log("Completed, " + x + c);
                        yield return new WaitForSeconds(2.0f);
                        SceneManager.LoadScene("Menu");
                        break;
                    }
                    yield return new WaitForSeconds(2.0f);                  
                    endScreen.SetActive(true);
                    Debug.Log(x + "   -   " + c);
                    break;
                }               
               
                yield return new WaitForSeconds(curScene.waitTime);
                fio.FadeOut();
                yield return new WaitForSeconds(2.0f);
                
                break;

            case 10://camera zoom
                GameObject.Find("Main Camera").GetComponent<CameraScript>().CameraZoom(curScene.zoomPosition, curScene.zoomAmount, curScene.reset, curScene.dramaCounter);
                yield return new WaitForSeconds(curScene.waitTime);
                NextScene();
                break;

            case 11://animation
                for(int z = 0; z < curScene.charactersInScene.Length; z++)
                {
                    if(curScene.isAnimated[z])
                    {
                        StartCoroutine(StartAnimation(curScene.charactersInScene[z], curScene.animationId[z], curScene.secondaryWaitTime[z], curScene.loopAnimation[z], curScene.isAnimated[z]));
                    }
                }
                yield return new WaitForSeconds(curScene.waitTime);
                NextScene();
                break;
            case 12://move camera

                break;
            case 13: //phone message
                phones.OpenPhone();
                phones.ShowNewMessage(curScene.characterName, curScene.dialogue, curScene.isEnd);
                break;
            case 14: //tooltip
                tt.OpenTooltip(curScene.dialogue);
                break;         
        }     
    }

    IEnumerator StartAnimation(string whotomove, int animationid, float waitTimes, bool loop, bool isanimated)
    {
        //starts the animations
        animations = new string[] {"Idle", "Laugh", "Cry", "Angry", "Agree", "Disagree", "Shrug", "Talk"};
        GameObject go = new GameObject();
        string s = "";
        bool skip = false;
        try
        {
            go = GameObject.Find(whotomove);   
        }
        catch
        {
            print("No player " + whotomove + " found");
            skip = true;
        }
        if(!skip)
        {
            if (go.GetComponent<CharacterSetup>().gender == 0)
            {
                s = "Male";
            }
            else
            {
                s = "Female";
            }
            s += animations[animationid];
            yield return new WaitForSeconds(waitTimes);
            go.GetComponent<Animation>().Stop();
            go.GetComponent<Animation>().Play(s);
        }     
    }
    public void EnableReturn()
    {
        optionWindow.SetActive(true);
    }
    public void DisableReturn()
    {
        optionWindow.SetActive(false);
    }
    public void GoHome()
    {
        StartCoroutine(GoingHome());
    }
    IEnumerator GoingHome()
    { 
        DisableReturn();
        fio.FadeOut();
        yield return new WaitForSeconds(2.0f);
        SceneManager.LoadScene("Menu");
    }
    public void EnableSelection()
    {
        //enables the selection window in dialogue script
        ds.EnableSelection(true, curScene.selectionOption1, curScene.selectionOption2, curScene.selectionOption3);
    }
    public void Selection(int i)
    {
        //selects the next scene to be loaded
        if(i == 1)
        {
            curScene.nextScenario = curScene.nextBranch1;
        }
        if (i == 2)
        {
            curScene.nextScenario = curScene.nextBranch2;
        }
        if(i == 3)
        {
            curScene.nextScenario = curScene.nextBranch3;
        }
    }
    void SetupScene()
    {
        //sets up the scenario
        GameObject go = new GameObject();
        Character c = new Character();
        backgroundImage.sprite = slib.backgroundSprites[curScene.backgroundCounter];
        //deletes existing character and spawns new ones.
        GameObject[] charys = GameObject.FindGameObjectsWithTag("Character");
        foreach (GameObject g in charys)
        {
            Destroy(g);
        }
        Debug.Log("Prefs: " + PlayerPrefs.GetInt("Custom"));
        Debug.Log(curScene.characterToSpawn.Length);
        if (curScene.characterToSpawn.Length > 0)
        {         
            for (int i = 0; i < curScene.characterToSpawn.Length; i++)
            {
                if(PlayerPrefs.GetInt("Custom") == 0)
                {
                    c = jsl.GetResourceCharacter(curScene.characterToSpawn[i]);
                }
                if (PlayerPrefs.GetInt("Custom") == 1)
                {
                    c = jsl.GetCharacter(curScene.characterToSpawn[i]);
                }
                if (c.gender == 0)
                {
                    go = Instantiate(malePrefab, curScene.startPositions[i], Quaternion.identity);
                }
                if(c.gender == 1)
                {
                    go = Instantiate(femalePrefab, curScene.startPositions[i], Quaternion.identity);
                }
             
                go.name = curScene.characterToSpawn[i];
                go.transform.position = new Vector3(curScene.startPositions[i].x, yspawn[curScene.layerCounter[i]], zspawn[curScene.layerCounter[i]]);
                go.GetComponent<CharacterSetup>().SetScales(lScale[curScene.layerCounter[i]]);
                go.GetComponent<CharacterSetup>().SetRotation(curScene.lookingRight[i]);
                go.GetComponent<CharacterSetup>().SetPosition(new Vector3(0,0,0), 1);
                foreach(Transform child in go.transform.GetComponentsInChildren<Transform>())
                {
                    child.GetComponent<SpriteRenderer>().sortingOrder = laye[curScene.layerCounter[i]];
                    if(curScene.layerCounter[i] > 1)
                    {
                        child.GetComponent<SpriteRenderer>().color = fadeGray;
                    }
                }
            }
        }
        
        
        Debug.Log(curScene.backgroundCounter);
        
        
    }
    IEnumerator MoveCharacter(string cname, Vector2 topos, float speed, bool toRight, float wait, bool lookingright)
    {
        //move character
        yield return new WaitForSeconds(wait);
        GameObject go = GameObject.Find(cname);
        go.GetComponent<CharacterSetup>().SetRotation(lookingright);
        go.GetComponent<CharacterSetup>().speed = speed;
        go.GetComponent<CharacterSetup>().SetPosition(new Vector3(topos.x, go.transform.position.y, 0), 0);
        
    }
 
    IEnumerator StartSelectionDialogue(bool isteller, string sname, string sdiag, string selection1, string selection2)
    {
        //starts the selection dialogue 
        dt.text = "selection start";
        if (!ds.isActiveAndEnabled)
        {
            yield return new WaitForSeconds(0.6f);
        }
        
        ds.SelectionActive(true);
        yield return new WaitForSeconds(0.20f);
        ds.StartChat(isteller, sname, curScene.selectionQuestion, true);
    }
    IEnumerator StartDialogue(bool isteller, string sname, string sdiag)
    {
        //starts the basic dialogue
        dt.text = "dialogue start";

        if (!ds.isActiveAndEnabled)
        {
            yield return new WaitForSeconds(0.6f);
        }
        
        ds.SetChatActive(true);
        yield return new WaitForSeconds(0.20f);
        ds.StartChat(isteller, sname, sdiag, false);
    }
	IEnumerator StartGame()
    {
        //starts the story
        dt.text = "game start";
        Debug.Log(PlayerPrefs.GetInt("LoadSave"));
        if(PlayerPrefs.GetInt("LoadSave") == 0)
        {
            curScene = jsl.GetResourceScenario(storyName, "A0");
        }
        if (PlayerPrefs.GetInt("LoadSave") == 1)
        {
            curScene = jsl.LoadSaveFile(storyName, "SaveSetup");
        }
        yield return new WaitForSeconds(0.1f);
        StartCoroutine(SelectNextScene(curScene.thisScenario, curScene.scenarioType));
    }
    public void ContinueStory()
    {
        SceneManager.LoadScene("GameScene");
    }
    public void GoToMenu()
    {
        SceneManager.LoadScene("Menu");
    }
}





// Below this there is a scenario class, which is basically every scene in the game. could have made an own script for it
// but it wasn't supposed to be this big at first




public class ScenarioClass
{
    //global values
    public string thisScenario;
    public string nextScenario;
    public int scenarioType;
    public float waitTime;
    public float[] secondaryWaitTime;
    public bool cameraFollow;
    public string unitySceneToLoad;
    public float startHappy;
    public float curHappy;
    public float reqHappy;
    public float addHappy;
    public string[] charactersInScene;
    public bool isEnd;
    public int audioClip;
    public int[] layerCounter;
    //fade values, TYPE: 0
    public bool fadeIn;
    public int backgroundCounter;

    // dialogue values, TYPE: 1
    public string characterName;
    public string dialogue;
    public bool isTeller;

    //selction values, TYPE: 2
    public string selectionQuestion;
    public string nextBranch1, nextBranch2, nextBranch3;
    public string selectionOption1, selectionOption2, selectionOption3;

    //move values, TYPE: 3
    public bool[] moveThis;//this value is also in animation value
    public bool[] lookingRight;
    public Vector2[] moveTo;
    public float moveSpeed;
    public bool turnCharacter;
    public Vector2[] savePosition;

    //animate values, TYPE: 4
    public int[] animationId;
    public bool[] loopAnimation;
    public bool[] isAnimated;
    public string[] characterToSpawn;
    public Vector2[] startPositions;

    //change clothing
    public int clotheid;
    public int subid;

    //overlaycutscene
    public int selectedCutscene;

    //camera zoom values
    public float zoomAmount;
    public Vector2 zoomPosition;
    public float zoomSpeed;
    public bool reset;
    public int dramaCounter;

    //setup Scene
    public ScenarioClass(string sceneId, string nextSceneId, int typeId, int scenebg, string[] charrus, bool[] toright, Vector2[] charspawnPos, bool fade, float wait)
    {
        thisScenario = sceneId;
        nextScenario = nextSceneId;
        scenarioType = typeId;
        backgroundCounter = scenebg;
        lookingRight = toright;
        characterToSpawn = charrus;
        startPositions = charspawnPos;
        fadeIn = fade;
        waitTime = wait;
    }
    //fade
    public ScenarioClass(string sceneId, string nextSceneId, int typeId, bool fade, float wait)
    {
        thisScenario = sceneId;
        nextScenario = nextSceneId;
        scenarioType = typeId;
        fadeIn = fade;
        waitTime = wait;
    }
    //dialog
    public ScenarioClass(string sceneId, string nextSceneId, int typeId, string cname, string sdialogue, bool teller)
    {
        thisScenario = sceneId;
        nextScenario = nextSceneId;
        scenarioType = typeId;
        characterName = cname;
        dialogue = sdialogue;
        isTeller = teller;
    }
    //selection
    public ScenarioClass(string sceneId, string nextSceneId, int typeId, string selectionQ, string next1, string next2, string next1Option, string next2Option)
    {
        nextScenario = "";
        thisScenario = sceneId;
        scenarioType = typeId;
        nextBranch1 = next1;
        nextBranch2 = next2;
        isTeller = true;
        selectionQuestion = selectionQ;
        selectionOption1 = next1Option;
        selectionOption2 = next2Option;
    }
    //move
    public ScenarioClass(string sceneId, string nextSceneId, int typeId, string[] characters, bool[] ismoving, bool[] lookright, Vector2[] newpos, float[] swTime, float mSpeed, float wait)
    {
        thisScenario = sceneId;
        scenarioType = typeId;
        charactersInScene = characters;
        nextScenario = nextSceneId;
        moveThis = ismoving;
        lookingRight = lookright;
        moveTo = newpos;
        secondaryWaitTime = swTime;
        waitTime = wait;
        moveSpeed = mSpeed;
    }
    //animate
    public ScenarioClass(string sceneId, string nextSceneId, int typeId, string[] characters, bool[] isAnim, int[] whatanime, float wait, float[] swTime, bool[] isLoop)
    {
        thisScenario = sceneId;
        scenarioType = typeId;
        nextScenario = nextSceneId;
        charactersInScene = characters;
        isAnimated = isAnim;
        secondaryWaitTime = swTime;
        animationId = whatanime;
        waitTime = wait;
        loopAnimation = isLoop;
    }
    //animate and dialogue
    public ScenarioClass(string sceneId, string nextSceneId, int typeId, string cname, string sdialogue, bool teller, string[] characters, float[] swTime, bool[] isAnim, int[] whatanime, bool[] isLoop)
    {
        thisScenario = sceneId;
        nextScenario= nextSceneId;
        scenarioType = typeId;
        characterName = cname;
        dialogue = sdialogue;
        charactersInScene = characters;
        isTeller = teller;
        secondaryWaitTime = swTime;
        isAnimated = isAnim;
        animationId = whatanime;
        loopAnimation = isLoop;
    }
    //move and dialogue
    public ScenarioClass(string sceneId, string nextSceneId, int typeId, string cname, string sdialogue, bool teller, float[] swTime, bool[] movethis, Vector2[] topos)
    {
        thisScenario = sceneId;
        nextScenario = nextSceneId;
        scenarioType = typeId;
        characterName = cname;
        dialogue = sdialogue;
        isTeller = teller;
        secondaryWaitTime = swTime;
        moveThis = movethis;
        moveTo = topos;
    }
    //cutscene overlay Image
    public ScenarioClass(string sceneId, string nextSceneId, int typeId, float duration, int selected)
    {
        thisScenario = sceneId;
        nextScenario = nextSceneId;
        scenarioType = typeId;
        waitTime = duration;
        selectedCutscene = selected;
    }
    //change clothing
    public ScenarioClass(string sceneId, string nextSceneId, int typeId , string cname, int cl, int scl, int wait)
    {
        clotheid = cl;
        subid = scl;
        thisScenario = sceneId;
        nextScenario = nextSceneId;
        characterName = cname;
        waitTime = wait;
        scenarioType = typeId;
    }
    //gameEnd
    public ScenarioClass(string sceneId, string scene, int typeId, float wait)
    {
        thisScenario = sceneId;
        unitySceneToLoad = scene;
        waitTime = wait;
        scenarioType = typeId;
        isEnd = true;
    }
    //camera zoom
    public ScenarioClass(string sceneId, string nextSceneId, int typeId, float zoom, bool resetti, Vector2 zoomPos, float wait, float speed)
    {
        thisScenario = sceneId;
        nextScenario = nextSceneId;
        scenarioType = typeId;
        zoomAmount = zoom;
        reset = resetti;
        zoomPosition = zoomPos;
        waitTime = wait;
        zoomSpeed = speed;
    }   
    //empty constructor
    public ScenarioClass()
    {

    }

}