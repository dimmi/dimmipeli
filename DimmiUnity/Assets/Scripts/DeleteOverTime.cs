﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteOverTime : MonoBehaviour {

    public float f;

	// Use this for initialization
	void Start ()
    {
        StartCoroutine(DieOT());	
	}
    
    IEnumerator DieOT()
    {
        yield return new WaitForSeconds(f);
        Destroy(gameObject);
    }
}
