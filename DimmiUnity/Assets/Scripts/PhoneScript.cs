﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhoneScript : MonoBehaviour {


    public GameObject phone;

    public int messagenro;
    public Image[] messageboxes;
    public Text[] messagetexts;
    public Text[] nametexts;

    public float visibility;
    public bool opened;
    public GameScript gamescript;
	// Use this for initialization
	void Start ()
    {
        visibility = 0f;
        opened = false;
        ClearPhone();
        foreach(Text t in messagetexts)
        {
            t.text = "";
        }
        foreach(Text t in nametexts)
        {
            t.text = "";
        }
	}
	
	// Update is called once per frame
	void Update () 
    {
        phone.GetComponent<CanvasGroup>().alpha = Mathf.Lerp(phone.GetComponent<CanvasGroup>().alpha, visibility, 4f * Time.deltaTime);
        gamescript = GetComponent<GameScript>();
    }

    public void ShowNewMessage(string cname, string newmessage, bool incoming)
    {
        //shows the message boxes and changes the text
        messageboxes[messagenro].gameObject.SetActive(true);
        messagetexts[messagenro].text = newmessage;
        nametexts[messagenro].text = cname;
        messageboxes[messagenro].transform.localPosition = new Vector2(incoming == true ? -58 : 58, messageboxes[messagenro].transform.localPosition.y);

        messagenro++;
    }
    public void OpenPhone()
    {
        //shows the phone
        opened = true;
        visibility = 1f;
        phone.GetComponent<CanvasGroup>().interactable = true;
        phone.GetComponent<CanvasGroup>().blocksRaycasts = true;
        for(int i = 0; i < messageboxes.Length; i++)
        {
            messageboxes[i].gameObject.SetActive(i < messagenro);
        }     
    }
    public void ClosePhone()
    {
        //hides the phone
        opened = false;
        visibility = 0f;
        phone.GetComponent<CanvasGroup>().interactable = false;
        phone.GetComponent<CanvasGroup>().blocksRaycasts = false;
        ClearPhone();
    }
    public void ClearPhone()
    {
        //clears all the text messages
        messagenro = 0;
        foreach(Text t in nametexts)
        {
            t.text = "";
        }
        foreach(Text t in messagetexts)
        {
            t.text = "";
        }
        foreach(Image i in messageboxes)
        {
            i.gameObject.SetActive(false);
        }
    }
    public void NextScene()
    {
        //next scene
        gamescript.NextScene();
    }

    IEnumerator Flashing()
    {
        yield return new WaitForSeconds(1.0f);
    }
}
