﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeInOut : MonoBehaviour {

    public bool fadeIn;
    public bool fading;
    public float fadeTimer;
    public Color c1; //transparent
    public Color c2; //black
    public Image i;

	// Use this for initialization
	void Start ()
    {
        i = GetComponent<Image>();
	}
    IEnumerator WaitTimer(float f)
    {
        yield return new WaitForSeconds(f);
        FadeIn();
    }
	// Update is called once per frame
	void Update ()
    {
        //fades in and out
		if(fadeIn)
        {
            i.color = Color.Lerp(i.color, c1, 4f * Time.deltaTime);
        }
        if(!fadeIn)
        {
            i.color = Color.Lerp(i.color, c2, 4f * Time.deltaTime);
        }
	}
    public void FadeIn()
    {
        //in
        fading = true;
        fadeIn = true;
    }
    public void FadeOut()
    {
        //out
        fading = true;
        fadeIn = false;
    }
}
