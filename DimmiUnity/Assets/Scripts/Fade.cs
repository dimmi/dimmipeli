﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fade : MonoBehaviour {

    public bool isImage;
    public bool isText;

    private Image img;
    private Text txt;

    public Color startColor;
    public Color endColor;

    public bool lerping;
	// Use this for initialization
	void Start ()
    {
        StartCoroutine(StartLerp());
		if(isImage)
        {
            img = GetComponent<Image>();
            startColor = img.color;
            endColor = startColor;
            endColor.a = 0;
        }
        if(isText)
        {
            txt = GetComponent<Text>();
            startColor = txt.color;
            endColor = startColor;
            endColor.a = 0;
        }
	}
    IEnumerator StartLerp()
    {
        yield return new WaitForSeconds(1.0f);
        lerping = true;
        yield return new WaitForSeconds(4.0f);

    }
	// Update is called once per frame
	void Update ()
    {
       
        if(lerping)
        {
            if (isImage)
            {
                img.color = Color.Lerp(img.color, endColor, 3f * Time.deltaTime);
            }
            if (isText)
            {
                txt.color = Color.Lerp(txt.color, endColor, 3f * Time.deltaTime);
            }
        }
		
	}
}
