﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Bugsy;

public class SpriteLibrary : MonoBehaviour{

    //library of all the sprites
    public Sprite[] manHair, manhatHair, manlefteye1, manlefteye2, manlefteye3, manrighteye1, manrighteye2, manrighteye3, manmouth, manMouth1, manMouth2, manMouth3, manMouth4, manMouth5, manMouth6, manbrow1, manbrow2, manNose,
                    womanHair1, womanHair2, womanHair3, womanbrowleft, womanbrowright, womanlefteye1, womanlefteye2, womanlefteye3, womanrighteye1, womanrighteye2, womanrighteye3, womanmouth, womanMouth1, womanMouth2, womanMouth3, womanMouth4, womanMouth5, womanMouth6, womanNose;
    public Sprite[] womanSkin1, womanSkin2, womanSkin3, manSkin1, manSkin2, manSkin3, womanAllSkins, manAllSkins;
    public Sprite[] backgroundSprites;
    public Sprite[] manshirt1, manshirt2, manpant1, manshoes, manhat, manAllClothes;
    public Sprite[] womanshirt1, womanshirt2, womanpant1, womanshoes, womanhat, womanAllClothes;
    public Sprite[] logo;
    public Sprite[] old;
    public Sprite[] cutSceneImages;
    public Sprite endScreen;
    public Sprite[] temp;
    public int counter;
    public int counter2;

    // Use this for initialization
    void Awake()
    {
        //gets all sprites on awake so every script can use them
        backgroundSprites = Resources.LoadAll<Sprite>("Backgrounds") as Sprite[];
        logo = Resources.LoadAll<Sprite>("Clothes/Logo") as Sprite[];

        manHair = Resources.LoadAll<Sprite>("BodyParts/Man/Hair/Main") as Sprite[];
        manhatHair = Resources.LoadAll<Sprite>("BodyParts/Man/Hair/Hat") as Sprite[];
        manmouth = Resources.LoadAll<Sprite>("BodyParts/Man/Mouth") as Sprite[];
        manSkin1 = Resources.LoadAll<Sprite>("BodyParts/Man/Body/Vaalea") as Sprite[];
        manSkin2 = Resources.LoadAll<Sprite>("BodyParts/Man/Body/Keski") as Sprite[];
        manSkin3 = Resources.LoadAll<Sprite>("BodyParts/Man/Body/Tumma") as Sprite[];
        manAllSkins = Resources.LoadAll<Sprite>("BodyParts/Man/Body") as Sprite[];
        manNose = Resources.LoadAll<Sprite>("BodyParts/Man/Nose") as Sprite[];
        womanHair1 = Resources.LoadAll<Sprite>("BodyParts/Woman/Hair/Main") as Sprite[];
        womanHair2 = Resources.LoadAll<Sprite>("BodyParts/Woman/Hair/Front") as Sprite[];
        womanHair3 = Resources.LoadAll<Sprite>("BodyParts/Woman/Hair/Back") as Sprite[];
        womanmouth = Resources.LoadAll<Sprite>("BodyParts/Woman/Mouth") as Sprite[];
        womanSkin1 = Resources.LoadAll<Sprite>("BodyParts/Woman/Body/Vaalea") as Sprite[];
        womanSkin2 = Resources.LoadAll<Sprite>("BodyParts/Woman/Body/Keski") as Sprite[];
        womanSkin3 = Resources.LoadAll<Sprite>("BodyParts/Woman/Body/Tumma") as Sprite[];
        womanAllSkins = Resources.LoadAll<Sprite>("BodyParts/Woman/Body") as Sprite[];
        womanNose = Resources.LoadAll<Sprite>("BodyParts/Woman/Nose") as Sprite[];

        womanshirt1 = Resources.LoadAll<Sprite>("Clothes/Woman/Top/Shirt1") as Sprite[];
        womanshirt2 = Resources.LoadAll<Sprite>("Clothes/Woman/Top/Shirt2") as Sprite[];
        womanpant1 = Resources.LoadAll<Sprite>("Clothes/Woman/Bottom/Pants1") as Sprite[];
        womanshoes = Resources.LoadAll<Sprite>("Clothes/Unisex/Shoe") as Sprite[];
        womanhat = Resources.LoadAll<Sprite>("Clothes/Unisex/Shoe") as Sprite[];
        manshirt1 = Resources.LoadAll<Sprite>("Clothes/Man/Top/Shirt1") as Sprite[];
        manshirt2 = Resources.LoadAll<Sprite>("Clothes/Man/Top/Shirt2") as Sprite[];
        manpant1 = Resources.LoadAll<Sprite>("Clothes/Man/Bottom/Pants1") as Sprite[];
        manshoes = Resources.LoadAll<Sprite>("Clothes/Unisex/Shoe") as Sprite[];
        manhat = Resources.LoadAll<Sprite>("Clothes/Unisex/Cap") as Sprite[];
        old = Resources.LoadAll<Sprite>("BodyParts/Old") as Sprite[];
        womanAllClothes = Debugsy.CombineArrays(Resources.LoadAll<Sprite>("Clothes/Unisex") as Sprite[], Resources.LoadAll<Sprite>("Clothes/Woman") as Sprite[]);
        manAllClothes = Debugsy.CombineArrays(Resources.LoadAll<Sprite>("Clothes/Unisex") as Sprite[], Resources.LoadAll<Sprite>("Clothes/Man") as Sprite[]);
        #region womanbrows
        temp = Resources.LoadAll<Sprite>("BodyParts/Woman/Hair/Brow") as Sprite[];
        counter = 0;
        counter2 = 0;
        womanbrowright = new Sprite[temp.Length / 2];
        womanbrowleft = new Sprite[temp.Length / 2];
        for (int i = 0; i < temp.Length; i++)
        {
            if (counter == 0)
            {
                womanbrowleft[counter2] = temp[i];
            }
            if (counter == 1)
            {
                womanbrowright[counter2] = temp[i];
            }
            counter++;
            if (counter > 1)
            {
                counter = 0;
                counter2++;
            }
        }
        #endregion
        #region manbrows
        temp = Resources.LoadAll<Sprite>("BodyParts/Man/Hair/Brow") as Sprite[];
        counter = 0;
        counter2 = 0;
        manbrow1 = new Sprite[temp.Length / 2];
        manbrow2 = new Sprite[temp.Length / 2];
        for(int x = 0; x < temp.Length; x++)
        {
            if(counter == 0)
            {
                manbrow1[counter2] = temp[x];
            }
            if(counter == 1)
            {
                manbrow2[counter2] = temp[x];
            }
            counter++;
            if(counter > 1)
            {
                counter = 0;
                counter2++;
            }

        }
        #endregion

        #region maneyes
        temp = Resources.LoadAll<Sprite>("BodyParts/Man/Eyes/Open") as Sprite[];
        counter = 0;
        counter2 = 0;
        manrighteye1 = new Sprite[temp.Length / 2];
        manlefteye1 = new Sprite[temp.Length / 2];
        for(int i = 0; i < temp.Length; i++)
        {
            if (counter == 0)
            {
                manlefteye1[counter2] = temp[i];
            }
            if(counter == 1)
            {
                manrighteye1[counter2] = temp[i];
            }
            counter++;
            if(counter > 1)
            {
                counter = 0;
                counter2++;
            }
        }
        temp = Resources.LoadAll<Sprite>("BodyParts/Man/Eyes/Mid") as Sprite[];
        counter = 0;
        counter2 = 0;
        manrighteye2 = new Sprite[temp.Length / 2];
        manlefteye2 = new Sprite[temp.Length / 2];
        for (int i = 0; i < temp.Length; i++)
        {
            if (counter == 0)
            {
                manlefteye2[counter2] = temp[i];
            }
            if (counter == 1)
            {
                manrighteye2[counter2] = temp[i];
            }
            counter++;
            if (counter > 1)
            {
                counter = 0;
                counter2++;
            }
        }
        temp = Resources.LoadAll<Sprite>("BodyParts/Man/Eyes/Closed") as Sprite[];
        counter = 0;
        counter2 = 0;
        manrighteye3 = new Sprite[temp.Length / 2];
        manlefteye3 = new Sprite[temp.Length / 2];
        for (int i = 0; i < temp.Length; i++)
        {
            if (counter == 0)
            {
                manlefteye3[counter2] = temp[i];
            }
            if (counter == 1)
            {
                manrighteye3[counter2] = temp[i];
            }
            counter++;
            if (counter > 1)
            {
                counter = 0;
                counter2++;
            }
        }
        #endregion
        #region womaneyes
        temp = Resources.LoadAll<Sprite>("BodyParts/Woman/Eyes/Open") as Sprite[];
        counter = 0;
        counter2 = 0;
        womanrighteye1 = new Sprite[temp.Length / 2];
        womanlefteye1 = new Sprite[temp.Length / 2];
        for (int i = 0; i < temp.Length; i++)
        {
            if (counter == 0)
            {
                womanlefteye1[counter2] = temp[i];
            }
            if (counter == 1)
            {
                womanrighteye1[counter2] = temp[i];
            }
            counter++;
            if (counter > 1)
            {
                counter = 0;
                counter2++;
            }
        }

        temp = Resources.LoadAll<Sprite>("BodyParts/Woman/Eyes/Mid") as Sprite[];
        counter = 0;
        counter2 = 0;
        womanrighteye2 = new Sprite[temp.Length / 2];
        womanlefteye2 = new Sprite[temp.Length / 2];
        for (int i = 0; i < temp.Length; i++)
        {
            if (counter == 0)
            {
                womanlefteye2[counter2] = temp[i];
            }
            if (counter == 1)
            {
                womanrighteye2[counter2] = temp[i];
            }
            counter++;
            if (counter > 1)
            {
                counter = 0;
                counter2++;
            }
        }
        Sprite[] tempsa = Debugsy.CombineArrays(womanSkin1, womanSkin2);
        tempsa = Debugsy.CombineArrays(tempsa, womanSkin3);
        womanAllSkins = tempsa;
        
        temp = Resources.LoadAll<Sprite>("BodyParts/Woman/Eyes/Closed") as Sprite[];
        counter = 0;
        counter2 = 0;
        womanrighteye3 = new Sprite[temp.Length / 2];
        womanlefteye3 = new Sprite[temp.Length / 2];
        for (int i = 0; i < temp.Length; i++)
        {
            if (counter == 0)
            {
                womanlefteye3[counter2] = temp[i];
            }
            if (counter == 1)
            {
                womanrighteye3[counter2] = temp[i];
            }
            counter++;
            if (counter > 1)
            {
                counter = 0;
                counter2++;
            }
        }
        #endregion
   
    }
}