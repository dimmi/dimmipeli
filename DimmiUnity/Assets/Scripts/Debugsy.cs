﻿using System.Collections;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Bugsy
{
    public class Debugsy
    {
        /// <summary>
        /// A debugging script with few helpful functions to make your code cleaner and faster
        /// Made by: Jere Ahvenus TTV17SP
        /// </summary>

        
        public static void AddDropdownList(Dropdown d, List<string> l)
        {
            //creates the dropdown from list.
            d.ClearOptions();
            d.AddOptions(l);
        }
        public static void AddDropdownArray(Dropdown d, string[] s)
        {
            //creates the dropdown from array
            d.ClearOptions();
            d.AddOptions(ArrayToList(s));
        }
        public static string[] ListToArray(List<string> l)
        {
            //turns a string list to string array
            string[] s = new string[l.Count];
            for (int i = 0; i < l.Count; i++)
            {
                s[0] = l[0];
            }
            return s;
        }
        public static List<string> ArrayToList(string[] s)
        {
            //turns a string array to a list
            List<string> list = new List<string>() { };
            for (int i = 0; i < s.Length; i++)
            {
                list.Add(s[i]);
            }
            return list;
        }
        public static List<Sprite> ArrayToList(Sprite[] s)
        {
            //turns a sprite array to a list
            List<Sprite> list = new List<Sprite>() { };
            for (int i = 0; i < s.Length; i++)
            {
                list.Add(s[i]);
            }
            return list;
        }
        public static List<SpriteRenderer> ArrayToList(SpriteRenderer[] s)
        {
            //turns a spriterenderer array to a list
            List<SpriteRenderer> list = new List<SpriteRenderer>() { };
            for (int i = 0; i < s.Length; i++)
            {
                list.Add(s[i]);
            }
            return list;
        }
        public static List<int> ArrayToList(int[] s)
        {
            //turns a int array to a list
            List<int> list = new List<int>() { };
            for (int i = 0; i < s.Length; i++)
            {
                list.Add(s[i]);
            }
            return list;
        }
        public static List<string> CombineLists(List<string> s1, List<string> s2)
        {
            //combines two string lists into one
            List<string> combinedList = new List<string>() { };           
            for(int i1 = 0; i1 < s1.Count; i1++)
            {
                combinedList.Add(s1[i1]);
            }
            for(int i2 = 0; i2 < s2.Count; i2++)
            {
                combinedList.Add(s2[i2]);
            }
            return combinedList;
        }
        public static List<StoryList> CombineLists(List<StoryList> s1, List<StoryList> s2)
        {
            //combines two string lists into one
            List<StoryList> combinedList = new List<StoryList>() { };
            for (int i1 = 0; i1 < s1.Count; i1++)
            {
                combinedList.Add(s1[i1]);
            }
            for (int i2 = 0; i2 < s2.Count; i2++)
            {
                combinedList.Add(s2[i2]);
            }
            return combinedList;
        }
        public static string[] CombineArrays(string[] s1, string[] s2)
        {
            //combines two string arrays into one
            int combinedLength = s1.Length + s2.Length;
            string[] combinedArray = new string[combinedLength];
            for(int i = 0; i < s1.Length; i++)
            {
                combinedArray[i] = s1[i];
            }
            for(int i2 = s1.Length; i2 < combinedLength; i2++)
            {
                combinedArray[i2] = s2[i2 - s1.Length];
            }
            return combinedArray;
        }
        public static Sprite[] CombineArrays(Sprite[] s1, Sprite[] s2)
        {
            //combines two sprite arrays into one
            int combinedLength = s1.Length + s2.Length;
            Sprite[] combinedArray = new Sprite[combinedLength];
            for (int i = 0; i < s1.Length; i++)
            {
                combinedArray[i] = s1[i];
            }
            for (int i2 = s1.Length; i2 < combinedLength; i2++)
            {
                combinedArray[i2] = s2[i2 - s1.Length];
            }
            return combinedArray;
        }
        public static bool DirectoryCheck(string path)
        {
            //checks if a directory exists
            bool b = false;
            if(Directory.Exists(path))
            {
                b = true;
            }
            else
            {
                b = false;
            }
            return b;
        }
        
    }
}

