﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FpsCounter : MonoBehaviour {

    private Text t;
    private float deltaTime = 0.0f;

	// Use this for initialization
	void Start ()
    {
        t = GameObject.Find("FpsText").GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        //shows the fps counter
        deltaTime += (Time.unscaledDeltaTime - deltaTime) * 0.1f;
        t.text = "FPS " + (int)(1.0f / deltaTime);
	}
}
