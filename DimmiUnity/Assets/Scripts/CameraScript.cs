﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {

    public bool zoom;
    public float zoomAmount;
    public float startZoom;
    public Vector2 zoomPos;
    public Vector2 startPos;
    public float zoomSpeed;
    public int dramaCounter;
    public Camera cam;


	// Use this for initialization
	void Start ()
    {
        cam = GetComponent<Camera>();
        startPos = transform.position;
        zoomSpeed = 5f;
        startZoom = cam.orthographicSize;
	}
	
	// Update is called once per frame
	void Update ()
    {
        //zooms the camera
		if(zoom)
        {
            transform.position = Vector3.Lerp(transform.position, new Vector3(zoomPos.x, zoomPos.y, -10f), Time.deltaTime * zoomSpeed * dramaCounter);
            cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, zoomAmount, Time.deltaTime * zoomSpeed * dramaCounter);
        }
        if(!zoom)
        {
            transform.position = Vector3.Lerp(transform.position, new Vector3(startPos.x, startPos.y, -10f), Time.deltaTime * zoomSpeed * dramaCounter);
            cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, 5, Time.deltaTime * zoomSpeed * dramaCounter);
        }
	}
    public void CameraZoom(Vector2 posit, float amount, bool reset, int drama)
    {
        //zooms the camera
        zoom = !reset;
        zoomPos = posit;
        zoomAmount = amount;
        dramaCounter = drama;
    }
}
