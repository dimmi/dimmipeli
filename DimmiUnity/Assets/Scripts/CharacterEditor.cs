﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Bugsy;

public class CharacterEditor : MonoBehaviour {

    private SpriteLibrary spril;
    private int[] gearArray; // 0 , 1 , 2 , 3 , 4 shirt, 5 shirtcolor, 6 pants, 7 pantcolor, 8 shoecolor
    private int gender;


    public string characterName;

    
    public JsonSaveLoad jsl;
    public string loadName;

    public GameObject canvas1, canvas2;
    public GameObject panel1, panel2;
    public bool selectedView;
    public float viewChangeSpeed;
    public GameObject[] gearSelections;


    public FadeInOut fio;
    public InputField nameField;
    public bool b;
    public bool fb;
    public GameObject maleStuff;
    public GameObject femaleStuff;
    public GameObject man, woman;
    public Sprite[] femaleSkin1, femaleSkin2, femaleSkin3, maleSkin1, maleSkin2, maleSkin3;
    public SpriteRenderer[] maleParts;
    public SpriteRenderer[] femaleParts;
    public Dropdown charactersDd;
    public Dropdown genderDd;
    public GameObject[] selectionWindowsMale;
    public GameObject[] selectionWindowsFemale;
    public int[] maleSetup;
    public int[] femaleSetup;
    public int femaleskincounter;
    public int maleskincounter;

    public List<SpriteRenderer> lsrmale;
    public List<SpriteRenderer> lsrfemale;

    public Sprite[] sa;
    public Color[] clothingColoring;

    public Sprite emptySprite;

    // Use this for initialization
    void Start ()
    {
        //setup
        spril = GetComponent<SpriteLibrary>();
        maleSetup = new int[12];
        femaleSetup = new int[12];
        maleStuff = GameObject.Find("MaleStuff");
        fio = GameObject.Find("FadeInOut").GetComponent<FadeInOut>();
        StartCoroutine(Fading(true));
        jsl = GameObject.Find("ScriptBlock").GetComponent<JsonSaveLoad>();
        charactersDd.ClearOptions();
        List<string> sl = jsl.GetCharacterNames();
        charactersDd.AddOptions(sl);
        
        lsrmale = Debugsy.ArrayToList(maleParts);
        lsrfemale = Debugsy.ArrayToList(femaleParts);
        maleSkin1 = spril.manSkin1;
        maleSkin2 = spril.manSkin2;
        maleSkin3 = spril.manSkin3;

        lsrmale.Clear();
        lsrfemale.Clear();
        foreach(Transform t in man.transform.GetComponentsInChildren<Transform>())
        {
            lsrmale.Add(t.GetComponent<SpriteRenderer>());
        }
        foreach (Transform t in woman.transform.GetComponentsInChildren<Transform>())
        {
            lsrfemale.Add(t.GetComponent<SpriteRenderer>());
        }
    }

	IEnumerator Fading(bool b)
    {
        //fade in out calls
        if(b)
        {   
            yield return new WaitForSeconds(1.0f);
            fio.FadeIn();
        }
        if (!b)
        {
            fio.FadeOut();
            yield return new WaitForSeconds(2.0f);
            SceneManager.LoadScene("Menu");
        }
    }
    public void SetGender()
    {
        //sets gender when loading character
        if (gender == 0)
        {
            man.SetActive(true);
            maleStuff.SetActive(true);
            femaleStuff.SetActive(false);
            woman.SetActive(false);
        }
        if (gender == 1)
        {
            man.SetActive(false);
            maleStuff.SetActive(false);
            femaleStuff.SetActive(true);
            woman.SetActive(true);
        }
    }
    public void ChangeGender()
    {
        //change gender from button
        gender = genderDd.value;
        if(gender == 0)
        {
            man.SetActive(true);
            maleStuff.SetActive(true);
            femaleStuff.SetActive(false);
            woman.SetActive(false);
        }
        if(gender == 1)
        {
            man.SetActive(false);
            maleStuff.SetActive(false);
            femaleStuff.SetActive(true);
            woman.SetActive(true);
        }
    }
    public void EquipGear()
    {
        if (gender == 0)
        {
            ChangeHair(maleSetup[0]);
            ChangeEye(maleSetup[1]);
            ChangeSkinTone(maleSetup[3]);
            ChangeMouth(maleSetup[2]);
            ChangeShoeColor(maleSetup[8]);
            ChangePantsColor(maleSetup[7]);
            ChangeShirt(maleSetup[4]);
            ChangeShirtColor(maleSetup[5]);
        }
        if (gender == 1)
        {
            ChangeHair(femaleSetup[0]);
            ChangeEye(femaleSetup[1]);
            ChangeSkinTone(femaleSetup[3]);
            ChangeMouth(femaleSetup[2]);
            ChangeShoeColor(femaleSetup[8]);
            ChangePantsColor(femaleSetup[7]);
            ChangeShirt(femaleSetup[4]);
            ChangeShirtColor(femaleSetup[5]);
        }
    }
    public void RandomGear()
    {
        //randomize character according to gender
        if (gender == 0)
        {
            ChangeHair(Random.Range(0, spril.manHair.Length));
            ChangeEye(Random.Range(0, spril.manlefteye1.Length));
            ChangeSkinTone(Random.Range(0, 3));
            ChangeMouth(Random.Range(0, spril.manmouth.Length/6));
            ChangeShoeColor(Random.Range(0, clothingColoring.Length));
            ChangePantsColor(Random.Range(0, clothingColoring.Length));
            ChangeShirt(Random.Range(1, 3));
            ChangeShirtColor(Random.Range(0, clothingColoring.Length));
        }   
        if (gender == 1)
        {
            ChangeHair(Random.Range(0, spril.womanHair1.Length));
            ChangeEye(Random.Range(0, spril.womanlefteye1.Length/7));
            ChangeMouth(Random.Range(0, spril.womanmouth.Length/6));
            ChangeSkinTone(Random.Range(0, 3));
            ChangeShirt(Random.Range(1, 2));
            ChangeShoeColor(Random.Range(0, clothingColoring.Length));
        }
    }
    public void ChangeHair(int i)
    {    
        //change hair
        if (gender == 0)
        {
            lsrmale.Find(x => x.name == "MainHair").sprite = spril.manHair[i];
            lsrmale.Find(x => x.name == "Brow1").sprite = spril.manbrow1[i];
            lsrmale.Find(x => x.name == "Brow2").sprite = spril.manbrow2[i];
        }
        if(gender == 1)
        {
            lsrfemale.Find(x => x.name == "MainHair").sprite = spril.womanHair1[i];
            lsrfemale.Find(x => x.name == "HairFront").sprite = spril.womanHair2[i];
            lsrfemale.Find(x => x.name == "BackHair").sprite = spril.womanHair3[i];
            lsrfemale.Find(x => x.name == "Brow1").sprite = spril.womanbrowleft[i];
            lsrfemale.Find(x => x.name == "Brow2").sprite = spril.womanbrowright[i];
        }
        UpdateGearArray(0, i);
    }
    public void ChangeMouth(int i)
    {
        //changes mouth
        if(gender == 0)
        {
            lsrmale.Find(x => x.name == "Mouth").sprite = spril.manmouth[i * 6 + 1];
        }
        if(gender == 1)
        {
            lsrfemale.Find(x => x.name == "Mouth").sprite = spril.womanmouth[i * 6 + 1];
        }
        UpdateGearArray(2, i);
    }
    public void ChangeEye(int i)
    {
        //changes eyes
        try
        {
            if (gender == 0)
            {
                lsrmale.Find(x => x.name == "Eye1").sprite = spril.manlefteye1[7 * maleskincounter + i];
                lsrmale.Find(x => x.name == "Eye2").sprite = spril.manrighteye1[7 * maleskincounter + i];
            }
            if (gender == 1)
            {
                lsrfemale.Find(x => x.name == "Eye1").sprite = spril.womanlefteye1[7 * femaleskincounter + i];
                lsrfemale.Find(x => x.name == "Eye2").sprite = spril.womanrighteye1[7 * femaleskincounter + i];
            }
            UpdateGearArray(1, i);
        }
        catch
        {
            print("error");
        }
    }
    public void ChangeSkinTone(int i)
    {
        //changes skin color
        print("skincounter" + i);
        List<Sprite> srlist;
        string tmp = "";
        if(gender == 0)
        {
            switch(i)
            {
                case 0:
                    sa = maleSkin1;
                    tmp = "L";
                    break;
                case 1:
                    sa = maleSkin2;
                    tmp = "M";
                    break;
                case 2:
                    sa = maleSkin3;
                    tmp = "D";
                    break;
            }
            srlist = Debugsy.ArrayToList(sa);

            lsrmale.Find(x => x.name == "Upperbody").sprite = srlist.Find(x => x.name == tmp + "Upperbody");
            lsrmale.Find(x => x.name == "LowerBody").sprite = srlist.Find(x => x.name == tmp + "LowerBody");
            lsrmale.Find(x => x.name == "RightUpperArm").sprite = srlist.Find(x => x.name == tmp + "RightUpperArm");
            lsrmale.Find(x => x.name == "RightLowerArm").sprite = srlist.Find(x => x.name == tmp + "RightLowerArm");
            lsrmale.Find(x => x.name == "RightHand").sprite = srlist.Find(x => x.name == tmp + "Hand");
            lsrmale.Find(x => x.name == "LeftHand").sprite = srlist.Find(x => x.name == tmp + "Hand");
            lsrmale.Find(x => x.name == "LeftLowerArm").sprite = srlist.Find(x => x.name == tmp + "LeftLowerArm");
            lsrmale.Find(x => x.name == "LeftUpperArm").sprite = srlist.Find(x => x.name == tmp + "LeftUpperArm");
            lsrmale.Find(x => x.name == "Head").sprite = srlist.Find(x => x.name == tmp + "Head");
            lsrmale.Find(x => x.name == "RightLeg").sprite = srlist.Find(x => x.name == tmp + "RightLeg");
            lsrmale.Find(x => x.name == "LeftLeg").sprite = srlist.Find(x => x.name == tmp + "LeftLeg");
            lsrmale.Find(x => x.name == "Ear").sprite = srlist.Find(x => x.name == tmp + "Ear");
            lsrmale.Find(x => x.name == "RightThigh").sprite = srlist.Find(x => x.name == tmp + "RightThigh");
            lsrmale.Find(x => x.name == "LeftThigh").sprite = srlist.Find(x => x.name == tmp + "LeftThigh");
            lsrmale.Find(x => x.name == "Nose").sprite = spril.manNose[i];

            maleskincounter = i;
            ChangeEye(maleSetup[1]);

        }
        if(gender == 1)
        {
            switch(i)   
            {
                case 0:
                    sa = femaleSkin1;
                    tmp = "L";
                    break;
                case 1:
                    sa = femaleSkin2;
                    tmp = "M";
                    break;
                case 2:
                    sa = femaleSkin3;
                    tmp = "D";
                    break;
            }
            srlist = Debugsy.ArrayToList(sa);

            lsrfemale.Find(x => x.name == "Upperbody").sprite = srlist.Find(x => x.name == tmp + "Upperbody");
            lsrfemale.Find(x => x.name == "LowerBody").sprite = srlist.Find(x => x.name == tmp + "LowerBody");
            lsrfemale.Find(x => x.name == "RightUpperArm").sprite = srlist.Find(x => x.name == tmp + "RightUpperArm");
            lsrfemale.Find(x => x.name == "RightLowerArm").sprite = srlist.Find(x => x.name == tmp + "RightLowerArm");
            lsrfemale.Find(x => x.name == "RightHand").sprite = srlist.Find(x => x.name == tmp + "Hand");
            lsrfemale.Find(x => x.name == "LeftHand").sprite = srlist.Find(x => x.name == tmp + "Hand");
            lsrfemale.Find(x => x.name == "LeftLowerArm").sprite = srlist.Find(x => x.name == tmp + "LeftLowerArm");
            lsrfemale.Find(x => x.name == "LeftUpperArm").sprite = srlist.Find(x => x.name == tmp + "LeftUpperArm");
            lsrfemale.Find(x => x.name == "Head").sprite = srlist.Find(x => x.name == tmp + "Head");
            lsrfemale.Find(x => x.name == "RightLeg").sprite = srlist.Find(x => x.name == tmp + "RightLeg");
            lsrfemale.Find(x => x.name == "LeftLeg").sprite = srlist.Find(x => x.name == tmp + "LeftLeg");
            lsrfemale.Find(x => x.name == "Ear").sprite = srlist.Find(x => x.name == tmp + "Ear");
            lsrfemale.Find(x => x.name == "RightThigh").sprite = srlist.Find(x => x.name == tmp + "RightThigh");
            lsrfemale.Find(x => x.name == "LeftThigh").sprite = srlist.Find(x => x.name == tmp + "LeftThigh");
            lsrfemale.Find(x => x.name == "Nose").sprite = spril.womanNose[i];

            femaleskincounter = i;
            ChangeEye(femaleSetup[1]);
        }
       
       
        UpdateGearArray(3,i);
    }     
    public void ChangeShirt(int i)
    {
        int counter = i;
        counter = i == 0 ? 1 : i;
        List<Sprite> shrt = new List<Sprite>() { };
        //changes shirt
        if(gender == 0)
        {
            switch(counter)
            {
                case 1:
                    shrt = Debugsy.ArrayToList(spril.manshirt1);
                    break;
                case 2:
                    shrt = Debugsy.ArrayToList(spril.manshirt2);
                    break;
            }
            lsrmale.Find(x => x.name == "LeftUpperSleeve").sprite = shrt.Find(x => x.name == "LeftUpperSleeve" + counter);
            lsrmale.Find(x => x.name == "RightUpperSleeve").sprite = shrt.Find(x => x.name == "RightUpperSleeve" + counter);
            lsrmale.Find(x => x.name == "ShirtTop").sprite = shrt.Find(x => x.name == "ShirtTop" + counter);
            lsrmale.Find(x => x.name == "ShirtBottom").sprite = shrt.Find(x => x.name == "ShirtBottom" + counter);
            try
            {
                lsrmale.Find(x => x.name == "LeftLowerSleeve").sprite = shrt.Find(x => x.name == "LeftLowerSleeve" + counter);
                lsrmale.Find(x => x.name == "RightLowerSleeve").sprite = shrt.Find(x => x.name == "RightLowerSleeve" + counter);
            }
            catch
            {

            }


        }
        if (gender == 1)
        {
            switch (counter)
            {
                case 1:
                    shrt = Debugsy.ArrayToList(spril.womanshirt1);
                    break;
                case 2:
                    shrt = Debugsy.ArrayToList(spril.womanshirt2);
                    break;
            }
            lsrfemale.Find(x => x.name == "LeftUpperSleeve").sprite = shrt.Find(x => x.name == "LeftUpperSleeve" + counter);
            lsrfemale.Find(x => x.name == "RightUpperSleeve").sprite = shrt.Find(x => x.name == "RightUpperSleeve" + counter);         
            lsrfemale.Find(x => x.name == "ShirtTop").sprite = shrt.Find(x => x.name == "ShirtTop" + counter);
            lsrfemale.Find(x => x.name == "ShirtBottom").sprite = shrt.Find(x => x.name == "ShirtBottom" + counter);

            try
            {
                lsrfemale.Find(x => x.name == "LeftLowerSleeve").sprite = shrt.Find(x => x.name == "LeftLowerSleeve" + counter);
                lsrfemale.Find(x => x.name == "RightLowerSleeve").sprite = shrt.Find(x => x.name == "RightLowerSleeve" + counter);
            }
            catch
            {

            }
        }
        UpdateGearArray(4,counter);
    }
    public void ChangeShirtColor(int i)
    {
        List<Sprite> pnts = new List<Sprite>() { };
        int counter = i == 0 ? 1 : i;
        //changes shirt color
        if (gender == 0)
        {
            switch(counter)
            {
                case 1:

                    break;
                case 2:

                    break;
            }
            lsrmale.Find(x => x.name == "LeftUpperSleeve").color = clothingColoring[i];
            lsrmale.Find(x => x.name == "RightUpperSleeve").color = clothingColoring[i];
            lsrmale.Find(x => x.name == "LeftLowerSleeve").color = clothingColoring[i];
            lsrmale.Find(x => x.name == "RightLowerSleeve").color = clothingColoring[i];
            lsrmale.Find(x => x.name == "ShirtTop").color = clothingColoring[i];
            lsrmale.Find(x => x.name == "ShirtBottom").color = clothingColoring[i];
        }
        if (gender == 1)
        {
            lsrfemale.Find(x => x.name == "LeftUpperSleeve").color = clothingColoring[i];
            lsrfemale.Find(x => x.name == "RightUpperSleeve").color = clothingColoring[i];
            lsrfemale.Find(x => x.name == "LeftLowerSleeve").color = clothingColoring[i];
            lsrfemale.Find(x => x.name == "RightLowerSleeve").color = clothingColoring[i];
            lsrfemale.Find(x => x.name == "ShirtTop").color = clothingColoring[i];
            lsrfemale.Find(x => x.name == "ShirtBottom").color = clothingColoring[i];

        }
        UpdateGearArray(5, i);
    }
    public void ChangePants(int i)
    {
        //change pant
        if (gender == 0)
        {
            lsrmale.Find(x => x.name == "LeftUpperPant").color = clothingColoring[i];
            lsrmale.Find(x => x.name == "RightUpperPant").color = clothingColoring[i];
            lsrmale.Find(x => x.name == "LeftLowerPant").color = clothingColoring[i];
            lsrmale.Find(x => x.name == "RightLowerPant").color = clothingColoring[i];
            lsrmale.Find(x => x.name == "PantWaist").color = clothingColoring[i];
        }
        if (gender == 1)
        {

        }
        UpdateGearArray(6, i);
    }
    public void ChangePantsColor(int i)
    {
        //change pant color
        if (gender == 0)
        {
            lsrmale.Find(x => x.name == "LeftUpperPant").color = clothingColoring[i];
            lsrmale.Find(x => x.name == "RightUpperPant").color = clothingColoring[i];
            lsrmale.Find(x => x.name == "LeftLowerPant").color = clothingColoring[i];
            lsrmale.Find(x => x.name == "RightLowerPant").color = clothingColoring[i];
            lsrmale.Find(x => x.name == "PantWaist").color = clothingColoring[i];
        }
        if (gender == 1)
        {
            lsrfemale.Find(x => x.name == "LeftUpperPant").color = clothingColoring[i];
            lsrfemale.Find(x => x.name == "RightUpperPant").color = clothingColoring[i];
            lsrfemale.Find(x => x.name == "LeftLowerPant").color = clothingColoring[i];
            lsrfemale.Find(x => x.name == "RightLowerPant").color = clothingColoring[i];
            lsrfemale.Find(x => x.name == "PantWaist").color = clothingColoring[i];
        }
        UpdateGearArray(7, i);
    }
    public void ChangeShoeColor(int i)
    {
        //change shoe color
        if (gender == 0)
        {
            lsrmale.Find(x => x.name == "RightFoot").color = clothingColoring[i];
            lsrmale.Find(x => x.name == "LeftFoot").color = clothingColoring[i];
        }
        if (gender == 1)
        {
            lsrfemale.Find(x => x.name == "RightFoot").color = clothingColoring[i];
            lsrfemale.Find(x => x.name == "LeftFoot").color = clothingColoring[i];
        }
        UpdateGearArray(8, i);
    }
    public void ChangeOld(int i)
    {
        if (gender == 0)
        {
            
            lsrmale.Find(x => x.name == "FaceDecal").sprite = i == 1 ? spril.old[0] : null;
            lsrmale.Find(x => x.name == "Beard").sprite = i == 1 ? spril.old[1] : null;
        }
        if (gender == 1)
        {
            lsrfemale.Find(x => x.name == "FaceDecal").sprite = i == 1 ? spril.old[0] : null;
        }
        UpdateGearArray(11, i);
    }
    public void ChangeLogo(int i)
    {
        if (gender == 0)
        {
            lsrmale.Find(x => x.name == "Logo").sprite = i == 1 ? spril.logo[0] : null;
        }
        if (gender == 1)
        {
            lsrfemale.Find(x => x.name == "Logo").sprite = i == 1 ? spril.logo[0] : null;
        }
        UpdateGearArray(10, i);
    }
    public void ChangeHat(int i)
    {
        if(gender == 0)
        {
            lsrmale.Find(x => x.name == "Hat").sprite = i == 1 ? spril.manhat[0] : null;
        }
        if(gender == 1)
        {
            lsrfemale.Find(x => x.name == "Hat").sprite = i == 1 ? spril.womanhat[0] : null;
        }
        UpdateGearArray(9, i);
    }
    public void ChangeFemaleStuff()
    {
        //changes the female menu states
        if(!fb)
        {
            femaleStuff.GetComponent<Animation>()["SwitchCharacterCreation"].speed = 1f;
            femaleStuff.GetComponent<Animation>().Play("SwitchCharacterCreation");
        }
        if(fb)
        {
            femaleStuff.GetComponent<Animation>()["SwitchCharacterCreation"].time = maleStuff.GetComponent<Animation>()["SwitchCharacterCreation"].length;
            femaleStuff.GetComponent<Animation>()["SwitchCharacterCreation"].speed = -1f;
            femaleStuff.GetComponent<Animation>().Play("SwitchCharacterCreation");
        }
        fb = !fb;
    }
    public void ChangeMaleStuff()
    {
        //changes the male menu states
       if(!b)
        {
            maleStuff.GetComponent<Animation>()["SwitchCharacterCreation"].speed = 1f;
            maleStuff.GetComponent<Animation>().Play("SwitchCharacterCreation");
        }
        if (b)
        {
            maleStuff.GetComponent<Animation>()["SwitchCharacterCreation"].time = maleStuff.GetComponent<Animation>()["SwitchCharacterCreation"].length;
            maleStuff.GetComponent<Animation>()["SwitchCharacterCreation"].speed = -1f;
            maleStuff.GetComponent<Animation>().Play("SwitchCharacterCreation");
        }

        b = !b;
    }
    public void SelectOpenWindow(int i)
    {     
        /* select which window is active */
        if(gender == 0)
        {
            for (int x = 0; x < selectionWindowsMale.Length; x++)
            {
                selectionWindowsMale[x].SetActive(x == i);
            }
        }
        if (gender == 1)
        {
            for (int x = 0; x < selectionWindowsFemale.Length; x++)
            {
                selectionWindowsFemale[x].SetActive(x == i);
            }
        }
    }
    public void ToMenu()
    {
        //call to go back to menu
        StartCoroutine(Fading(false));
    }

    public void SetArrays(string s, int i, int[] i2)
    {
        //sets the character class arrays
        characterName = s;
        gender = i;
        gearArray = i2;
        Debug.Log(characterName);
    }
    public void UpdateGearArray(int t, int c)
    {
        //updates the gear array so if you change the gender the setup for the other gender still stays
        if(gender == 0)
        {
            maleSetup[t] = c;
        }
        if(gender == 1)
        {
            femaleSetup[t] = c;
        }
    }
    public void LoadGearArray()
    {
        //loads the character
        Character c = jsl.GetCharacter(charactersDd.options[charactersDd.value].text);
        gender = c.gender;
        SetGender();
        if(gender == 0)
        {
            maleSetup = c.gear;
        }
        if (gender == 1)
        {
            femaleSetup = c.gear;
        }
        characterName = c.name;
        EquipGear();
    }
    public void OnNameChangeEnd()
    {
        //change the name setup
        characterName = nameField.text;
    }

    public void SaveCharacter()
    {
        //saves the character
        Character c = new Character();
        c.name = characterName;
        c.gender = gender;
        if(gender == 0)
        {
            c.gear = maleSetup;
        }
        if(gender == 1)
        {
            c.gear = femaleSetup;
        }
        jsl.SaveCharacterToJson(c);
    }

}
