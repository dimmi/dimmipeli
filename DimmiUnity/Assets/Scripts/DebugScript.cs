﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugScript : MonoBehaviour {

    public bool debuggingOthers;
    public bool debugDelete;
    public bool debugStoryName;
    private JsonSaveLoad jsl;
    public List<string> dl;
    public List<int> dc;
	// Use this for initialization
	void Awake ()
    {
        
        jsl = GetComponent<JsonSaveLoad>();
        
        if(debugDelete)
        {
            PlayerPrefs.DeleteAll();
            Debug.Log("Debug delete all script is active on gameobject ScriptBlock!");
        }
        if (debugStoryName)
        {
            Debug.Log("Setting selectedstory value to TestStory");
            PlayerPrefs.SetString("SelectedStory", "TestStory");
        }
        if (PlayerPrefs.GetInt("FirstTime") == 0)
        {
            
            jsl.FirstTimeSetup();
            PlayerPrefs.SetInt("FirstTime", 1);
        }
        
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void Start()
    {
        print("Start");
    }
    public void DEB(string s)
    {
        
        if (debuggingOthers)
        {
            Debug.Log(s);
        }
    }
    public void ADL(string s)
    {
        //this is a debug list you can show via text ui element
        //i use this on android when the debug console is not available
        for(int i = 0; i < dl.Count; i++)
        {
            if(s == dl[i])
            {
                dc[i] += 1;
                return;
            }
        }
        dl.Add(s);
        dc.Add(1);
        
    }
    
    public void AddDropdownList(Dropdown d, List<string> l)
    {
        //creates the dropdown from list.
        d.ClearOptions();
        d.AddOptions(l);
    }
    public void AddDropdownArray(Dropdown d, string[] s)
    {
        //creates the dropdown from array
        d.ClearOptions();
        d.AddOptions(ArrayToList(s));
    }
    public string[] ListToArray(List<string> l)
    {
        //turns a string list to string array
        string[] s = new string[l.Count];
        for(int i = 0; i < l.Count; i++)
        {
            s[0] = l[0];
        }
        return s;
    }
    public List<string> ArrayToList(string[] s)
    {
        //turns a string array to a list
        List<string> list = new List<string>() { };
        for(int i = 0; i < s.Length; i++)
        {
            list.Add(s[i]);
        }
        return list;
    }
    
}
