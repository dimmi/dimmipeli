﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tooltip : MonoBehaviour {

    public GameObject popup;
    public float alfa;
    public Text popuptext;
    public CanvasGroup cg;
    public GameScript gs;

	// Use this for initialization
	void Start ()
    {
        popup = GameObject.Find("PopUpCanvas");
        cg = popup.GetComponent<CanvasGroup>();
        popuptext = GameObject.Find("PopUpText").GetComponent<Text>();
        gs = GetComponent<GameScript>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        //indicates if the popup is visible or not
        cg.alpha = Mathf.Lerp(cg.alpha, alfa, 4f * Time.deltaTime);
	}
    
    public void OpenTooltip(string message)
    {
        //opens the "story teller" pop up window with the "message"
        popuptext.text = "" + message;
        alfa = 1f;
        cg.interactable = true;
        cg.blocksRaycasts = true;
    }

    public void CloseTooltip()
    {
        //closes the story teller popup
        alfa = 0f;
        cg.interactable = false;
        cg.blocksRaycasts = false;
        gs.NextScene();
    }

}
