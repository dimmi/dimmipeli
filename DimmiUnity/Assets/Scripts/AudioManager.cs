﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioManager : MonoBehaviour {

    public AudioSource auds;
    public AudioClip[] musics;
    public AudioClip lastPlayed;
    public int onoff;
    public bool stopped;
    public bool looping;
	// Use this for initialization
	void Start ()
    {
        //checks if the sounds is on/off
        auds = GetComponent<AudioSource>();
        onoff = PlayerPrefs.GetInt("Sounds");
        if(looping)
        {
            stopped = false;
            auds.loop = true;
            lastPlayed = musics[0];
            auds.clip = lastPlayed;
            auds.Play();
        }
        else
        {
            stopped = true;
        }
	}
	public void PlayMusicAmbient(int i)
    {
        //plays ambient music
        stopped = false;
        auds.loop = true;
        lastPlayed = musics[i];
        auds.Play();
    }
    public void PlayAmbient()
    {
        //plays the last played ambient
        stopped = false;
        auds.loop = true;
        lastPlayed = musics[0];
        auds.Play();
    }
    public void StopAll()
    {
        //stops all sounds
        stopped = true;
        auds.loop = false;
        auds.Stop();
    }
	// Update is called once per frame
	void Update ()
    {
      //if audio is not stopped but not played, plays last played sound
	  //  if(auds.isPlaying == false && stopped == false)
      //  {
      //      auds.PlayOneShot(lastPlayed);
      //  }
	}
}
