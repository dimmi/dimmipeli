﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SplashScreen : MonoBehaviour {

    public Animation a;
    // Use this for initialization
    void Awake()
    {
        Application.targetFrameRate = 30;
    }
    void Start ()
    {
        a = GetComponent<Animation>();
        StartCoroutine(SplashScreenTimer());	
	}
	
	IEnumerator SplashScreenTimer()
    {
        a.Play("FadeSplash");
        yield return new WaitForSeconds(2.0f);
        a.Play("SponsorAppear");
        yield return new WaitForSeconds(5.0f);
        a["FadeSplash"].speed = -1f;
        a["FadeSplash"].time = a["FadeSplash"].length;
        a.Play("FadeSplash");
        yield return new WaitForSeconds(2.0f);
        SceneManager.LoadScene("Menu");
    }
}
