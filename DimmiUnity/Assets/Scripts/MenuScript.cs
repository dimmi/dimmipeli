﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour {

    public JsonSaveLoad jsl;
    public FadeInOut fio;
    public InputField passcodeField;
    public Dropdown storySelection;
    public Dropdown customStorySelection;
    public string[] storyList;
    public List<string> stryList;
    public Toggle soundToggle;
    public GameObject menuButtons;
    public AudioSource auds;
    public AudioClip[] clips;
    public int counter;
    public int secondaryCounter;
    public bool b;

    public Text bug;

    // Use this for initialization
    void Start ()
    {
        //start setups
        bug = GameObject.Find("Bug").GetComponent<Text>();
        auds = GetComponent<AudioSource>();
        jsl = GameObject.Find("ScriptBlock").GetComponent<JsonSaveLoad>();
        fio = GameObject.Find("FadeInOut").GetComponent<FadeInOut>();
        menuButtons = GameObject.Find("UIBase");
        soundToggle = GameObject.Find("SoundToggle").GetComponent<Toggle>();
        if (PlayerPrefs.GetInt("Sounds") == 0)
        {
            soundToggle.isOn = false;
        }
        StartCoroutine(MenuLoad());
	}
    public void PlaySound(int i)
    {
        //plays the sound
        if(PlayerPrefs.GetInt("Sounds") == 1)
        {
            auds.PlayOneShot(clips[i]);
        }
    }
    public void Menuswap(int i)
    {
        //debug menu change
        if(i == 0)
        {
            SceneManager.LoadScene("Menu");
        }
        if(i == 1)
        {
            SceneManager.LoadScene("Menu2");
        }
    }
	public void ChangeSounds() 
    {
        //turns sounds on and off
        b = soundToggle.isOn;
        if (b)
        {
            PlayerPrefs.SetInt("Sounds", 1);
            GameObject.Find("AudioManager").GetComponent<AudioManager>().PlayAmbient();
            
        }
        if (!b)
        {
            PlayerPrefs.SetInt("Sounds", 0);
            GameObject.Find("AudioManager").GetComponent<AudioManager>().StopAll();
            auds.Stop();
            
        }
    }
    public void OpenHiddenLevel()
    {
        //opens the password protected level
        string s = passcodeField.text;
        Debug.Log(s);
        string lvl = "";
        if(s != "")
        {
            lvl = jsl.CheckPassword(s);
            Debug.Log(lvl);
            if (lvl != "")
            {
                PlayerPrefs.SetString("SelectedStory", lvl);
                StartCoroutine(Fading("GameScene"));
            }
        }     
        
    }
    IEnumerator Temp(int i)
    {
        //closes the firt and second phase
        Animation a = new Animation();
        PlayMenuAnimation(i);
        yield return new WaitForSeconds(1.02f);
        a = menuButtons.GetComponent<Animation>();
        a["MenuPlayAnimation2"].speed = -1f;
        a["MenuPlayAnimation2"].time = a["MenuPlayAnimation2"].length;
        menuButtons.GetComponent<Animation>().Play("MenuPlayAnimation2");
        counter = 0;
    }
    public void PlayMenuAnimation(int i)
    {
        //plays the menu animation
        Animation a = new Animation();
        switch(i)
        {
            case 1:          
                if(counter == 1)
                {
                    a = menuButtons.GetComponent<Animation>();
                    a["MenuPlayAnimation2"].speed = -1f;
                    a["MenuPlayAnimation2"].time = a["MenuPlayAnimation2"].length;
                    menuButtons.GetComponent<Animation>().Play("MenuPlayAnimation2");
                    counter = 0;
                    break;
                }
                if(counter > 4)
                {
                    //closes the characterselection and the newgame window
                    StartCoroutine(Temp(counter));
                    break;
                }
                if(counter != 1)
                {
                  
                    Debug.Log("0 -> 1");
                    a = menuButtons.GetComponent<Animation>();
                    a["StoryAnimation1"].time = 1f;
                    a["StoryAnimation1"].speed = 0f;
                    menuButtons.GetComponent<Animation>().Play("StoryAnimation1");
                    a["MenuPlayAnimation2"].speed = 1f;
                    menuButtons.GetComponent<Animation>().Play("MenuPlayAnimation2");
                    counter = i;
                }
                break;
            case 2:
                if (counter == 2)
                {
                    a = menuButtons.GetComponent<Animation>();
                    a["OptionsAnimation2"].speed = -1f;
                    a["OptionsAnimation2"].time = a["OptionsAnimation2"].length;
                    menuButtons.GetComponent<Animation>().Play("OptionsAnimation2");
                    counter = 0;
                    break;
                }
                if (counter != 2)
                {
                    a = menuButtons.GetComponent<Animation>();
                    a["OptionsAnimation2"].speed = 1f;
                    menuButtons.GetComponent<Animation>().Play("OptionsAnimation2");
                    counter = i;
                }
                break;
            case 3:
                if (counter == 3)
                {
                    a = menuButtons.GetComponent<Animation>();
                    a["EditAnimation2"].speed = -1f;
                    a["EditAnimation2"].time = a["EditAnimation2"].length;
                    menuButtons.GetComponent<Animation>().Play("EditAnimation2");
                    counter = 0;
                    break;
                }
                if (counter != 3)
                {
                    a = menuButtons.GetComponent<Animation>();
                    a["EditAnimation2"].speed = 1f;
                    menuButtons.GetComponent<Animation>().Play("EditAnimation2");
                    counter = i;
                }
                break;
            case 4:
                if (counter == 4)
                {
                    a = menuButtons.GetComponent<Animation>();
                    a["LoadAnimation2"].speed = -1f;
                    a["LoadAnimation2"].time = a["LoadAnimation2"].length;
                    menuButtons.GetComponent<Animation>().Play("LoadAnimation2");
                    counter = 0;
                    break;
                }
                if (counter != 4)
                {
                    a = menuButtons.GetComponent<Animation>();
                    a["LoadAnimation2"].speed = 1f;
                    menuButtons.GetComponent<Animation>().Play("LoadAnimation2");
                    counter = i;
                }
                break;
            case 5:
                if(counter == 5)
                {
                    a = menuButtons.GetComponent<Animation>();
                    a["StoryAnimation1"].speed = -1f;
                    a["StoryAnimation1"].time = a["StoryAnimation1"].length;
                    menuButtons.GetComponent<Animation>().Play("StoryAnimation1");
                    counter = 1;
                    break;
                }
                if(counter != 5)
                {
                    a = menuButtons.GetComponent<Animation>();
                    a["StoryAnimation1"].speed = 1f;
                    menuButtons.GetComponent<Animation>().Play("StoryAnimation1");
                    counter = i;
                }
                break;
            case 6:
                if (counter == 6)
                {
                    a = menuButtons.GetComponent<Animation>();
                    a["StoryAnimation2"].speed = -1f;
                    a["StoryAnimation2"].time = a["StoryAnimation2"].length;
                    menuButtons.GetComponent<Animation>().Play("StoryAnimation2");
                    counter = 1;
                    break;
                }
                if (counter != 6)
                {
                    a = menuButtons.GetComponent<Animation>();
                    a["StoryAnimation2"].speed = 1f;
                    menuButtons.GetComponent<Animation>().Play("StoryAnimation2");
                    counter = i;
                }
                break;
            case 7:
                if (counter == 7)
                {
                    a = menuButtons.GetComponent<Animation>();
                    a["StoryAnimation3"].speed = -1f;
                    a["StoryAnimation3"].time = a["StoryAnimation3"].length;
                    menuButtons.GetComponent<Animation>().Play("StoryAnimation3");
                    counter = 1;
                    break;
                }
                if (counter != 7)
                {
                    a = menuButtons.GetComponent<Animation>();
                    a["StoryAnimation3"].speed = 1f;
                    menuButtons.GetComponent<Animation>().Play("StoryAnimation3");
                    counter = i;
                }
                break;
            case 8:
                if (counter == 8)
                {
                    a = menuButtons.GetComponent<Animation>();
                    a["StoryAnimation4"].speed = -1f;
                    a["StoryAnimation4"].time = a["StoryAnimation4"].length;
                    menuButtons.GetComponent<Animation>().Play("StoryAnimation4");
                    counter = 1;
                    break;
                }
                if (counter != 8)
                {
                    a = menuButtons.GetComponent<Animation>();
                    a["StoryAnimation4"].speed = 1f;
                    menuButtons.GetComponent<Animation>().Play("StoryAnimation4");
                    counter = i;
                }
                break;
        }

     
    }
    IEnumerator Fading(string s)
    {
        //screen fade in out
        fio.FadeOut();
        yield return new WaitForSeconds(1.0f);
        SceneManager.LoadScene(s);           
    }
    public void DebugLoadScene()
    {
        //load scene
        StartCoroutine(Fading("GameScene"));
    }
    IEnumerator MenuLoad()
    {
        //enter the menu animation load
        yield return new WaitForSeconds(1.0f);
        fio.FadeIn();
        yield return new WaitForSeconds(0.5f);
        menuButtons.GetComponent<Animation>().Play("MenuAnimation");
    }
    public void GetAndLoad()
    {
        //calls the function that fades and loads the level, used from other scripts mostly.
        StartCoroutine(Fading("GameScene"));
    }
    public void LoadStory()
    {
        //loads the story
        string s2 = storySelection.options[storySelection.value].text;
        PlayerPrefs.SetString("SelectedStory", s2);
        PlayerPrefs.SetInt("CustomStory", 0);
        PlayerPrefs.SetInt("LoadSave", 0);
        StartCoroutine(Fading("GameScene"));
    }
    public void LoadStorySave()
    {
        //loads the saved story
        string s2 = storySelection.options[storySelection.value].text;
        PlayerPrefs.SetString("SelectedStory", s2);
        PlayerPrefs.SetInt("CustomStory", 0);
        PlayerPrefs.SetInt("LoadSave", 1);
        StartCoroutine(Fading("GameScene"));
    }
    public void LoadCustomStorySave()
    {
        //load saved custom story
        string s2 = customStorySelection.options[customStorySelection.value].text;
        PlayerPrefs.SetString("SelectedStory", s2);
        PlayerPrefs.SetInt("CustomStory", 1);
        PlayerPrefs.SetInt("LoadSave", 1);
        StartCoroutine(Fading("GameScene"));
    }
    public void LoadCustomStory()
    {
        //loads custom story
        string s2 = customStorySelection.options[customStorySelection.value].text;
        PlayerPrefs.SetString("SelectedStory", s2);
        PlayerPrefs.SetInt("CustomStory", 1);
        StartCoroutine(Fading("GameScene"));
    }
    public void LoadEditors(string s)
    {
        StartCoroutine(Fading(s));
    }


}
