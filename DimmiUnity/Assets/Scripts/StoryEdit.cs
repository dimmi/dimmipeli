﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Bugsy;

public class StoryEdit : MonoBehaviour {

    public SpriteLibrary slib;
    public List<string> alphabets;
    public bool isHiddenStory;
    public Canvas mainCanvas;
    public JsonSaveLoad jsl;
    public FadeInOut fio;
    public ScenarioClass scl;
    public List<bool> hiddenList;
    public int sceneType;

    public string storyName;

    public ScenarioClass[] sceneArray;

    public string[] branches;
    public int[] branchCounts;
    public int currentBranch;
    public int currentBranchScene;

    #region GlobalVariables
    public int[] characterLayer;
    public string[] charactersInStory;
    public bool[] characterIsMoved;
    public Vector2[] charactersInStorySpawnPosition;
    public Vector2[] characterMovePos;
    public float[] moveDelays;
    public bool[] isAnimated;
    public bool[] loopAnimation;
    public int[] animationCounters;
    public bool[] characterIsLookingRight;
    public float[] animateStartTimes;
    #endregion

    #region UIVariables


    //createstoryPanel
    public int storyCount;
    public Dropdown allStoriesDd;
    public InputField newStoryInput;
    public Toggle hiddenToggle;
    public InputField passwordInput;

    //panels
    public GameObject[] editorPanels;
    //header
    public Text storyNameText;
    public Dropdown allScenesDd;
    //for all
    public Dropdown thisBranchDd;
    public Dropdown thisSceneDd;
    public Dropdown nextBranchDd;
    public Dropdown nextSceneDd;
    public Dropdown secondaryBranchDd;//only available in selection as there are 2 options
    public Dropdown secondarySceneDd;//only available in selection as there are 2 options
    public Dropdown tetriaryBranchDd;
    public Dropdown tetriarySceneDd;
    public Dropdown sceneTypeDd;
    public Text nextSceneText;

    //fade
    public InputField fadeWaitInput;
    public Toggle fadeInToggle;

    //for dialogue
    public Toggle dialogueNarrator;
    public InputField dialogueInput;
    public InputField dialogueSpeaker; //0 is teller

    //for selection
    public InputField selectionInput;
    public InputField selectionOption1;
    public InputField selectionOption2;
    public InputField selectionOption3;
    public InputField nextSceneInput1;
    public InputField nextSceneInput2;
    public InputField nextSceneInput3;

    //for setup
    public Dropdown backgroundSetupDd;
    public Dropdown allCharactersDd;
    public Dropdown selectedCharacterDd;
    public Dropdown zLayer;
    public InputField selectedCharacterXpos;
    public InputField selectedCharacterYpos;
    public Toggle setupLookToggle;
    public Toggle setupToggle;
    public InputField setupWaitTime;

    //for move
    public Dropdown moveCharacterDd;
    public Toggle moveThisToggle;
    public Toggle isMovingLeft;
    public InputField moveToPosX;
    public InputField moveToPosY;
    public InputField movementSpeed;
    public InputField moveDelay;
    public InputField moveWait;

    //for end
    public InputField endWait;

    //for cutscene
    public Dropdown cutsceneDd;
    public InputField cutsceneWait;

    //for zoom
    public Toggle zoomReset;
    public InputField zoomSpeed;
    public InputField zoomAmount;
    public InputField zoomXPos;
    public InputField zoomYPos;
    public InputField zoomWait;

    //for animation
    public Dropdown whotoAnimateDd;
    public Dropdown whatAnimationDd;
    public Toggle isAnimatedToggle;
    public Toggle isLoopedAnimationToggle;
    public InputField secondaryAnimationWaitTime;
    public InputField animationWaitTime;

    //for playsound
    public Dropdown soundList;
    public InputField soundWait;

    //for animationdialogue
    public Dropdown dialWhoToAnimateDd;
    public Dropdown dialWhatAnimationDd;
    public Toggle dialIsAnimatedToggle;
    public Toggle dialIsLoopedToggle;
    public Toggle dialIsNarrator;
    public InputField dialName;
    public InputField dialDialogue;
    public InputField dialAnimationDelay;

    //for textmessage
    public Toggle receivingToggle;
    public InputField textmessageNameInput;
    public InputField textmessageInput;

    //for Tooltip
    public InputField tooltipmessageInput;
    #endregion
    public Text debtxt;

    // Use this for initialization
    void Start ()
    {
        debtxt = GameObject.Find("Debugtxt").GetComponent<Text>();
        debtxt.text = "1";
        slib = GetComponent<SpriteLibrary>();
        debtxt.text = "2";
        GetAllUIElements();
        debtxt.text = "3";
        nextSceneText = GameObject.Find("NextScenetext").GetComponent<Text>();
        debtxt.text = "4";
        secondaryBranchDd = GameObject.Find("SecondaryBranchDD").GetComponent<Dropdown>();
        debtxt.text = "5";
        secondarySceneDd = GameObject.Find("SecondaryBranchNumberDD").GetComponent<Dropdown>();
        debtxt.text = "6";
        fio = GameObject.Find("FadeInOut").GetComponent<FadeInOut>();
        debtxt.text = "7";
        jsl = GetComponent<JsonSaveLoad>();
        debtxt.text = "8";
        GetStuff();
        debtxt.text = "9";
        GetNameList();
        debtxt.text = "10";
        GetSceneList();
        debtxt.text = "11";
        //jsl.CheckHidden(allStoriesDd.options[allStoriesDd.value].text);
        debtxt.text = "12";
        StartCoroutine(StartFade());
        debtxt.text = "13";
    }
    void GetStuff()
    {
        storyCount = 0;
        List<string> ls = new List<string>();
        debtxt.text = "8.1";
        allStoriesDd = GameObject.Find("AllStoriesDropdown").GetComponent<Dropdown>();
        debtxt.text = "8.2";
        StoryList sli = new StoryList();
        sli = jsl.LoadStoryListClass(false);
        debtxt.text = "8.3";
        StoryList sli2 = new StoryList();
        sli2 = jsl.LoadStoryListClass(true);
        debtxt.text = "8.4";
        if(sli.story.Length > 0)
        {
            debtxt.text = "8.41";
            for (int i = 0; i < sli.story.Length; i++)
            {
                ls.Add(sli.story[i]);
                hiddenList.Add(sli.hidden[i]);
                storyCount++;
            }
        }
        debtxt.text = "8.5";
        if(sli2.story.Length > 0)
        {
            for (int i1 = 0; i1 < sli2.story.Length; i1++)
            {
                ls.Add(sli2.story[i1]);
                hiddenList.Add(sli2.hidden[i1]);
                storyCount++;
            }
        }
        debtxt.text = "8.6";
        allStoriesDd.ClearOptions();
        debtxt.text = "8.9";
        allStoriesDd.AddOptions(ls);
    }
    void GetNameList()
    {
        string[] s1 = jsl.LoadCharray();
        List<string> li = new List<string>();
        for (int i = 0; i < s1.Length; i++)
        {
            li.Add(s1[i]);
        }
        allCharactersDd.ClearOptions();
        allCharactersDd.AddOptions(li);
    }
    #region ValueChangeFunctions
    public void ChangeAnimateDiag()
    {
        dialIsAnimatedToggle.isOn = isAnimated[dialWhoToAnimateDd.value];
        dialIsLoopedToggle.isOn = loopAnimation[dialWhoToAnimateDd.value];
        dialWhatAnimationDd.value = animationCounters[dialWhoToAnimateDd.value];
        dialAnimationDelay.text = "" + moveDelays[dialWhoToAnimateDd.value];
    }
    public void ChangeAnimate()
    {
        isAnimatedToggle.isOn = isAnimated[whotoAnimateDd.value];
        isLoopedAnimationToggle.isOn = loopAnimation[whotoAnimateDd.value];
        whatAnimationDd.value = animationCounters[whotoAnimateDd.value];
        secondaryAnimationWaitTime.text = "" + animateStartTimes[whotoAnimateDd.value];
    }
    public void ChangeSetupRightLook()
    {
        characterIsLookingRight[selectedCharacterDd.value] = setupLookToggle.isOn;
    }
    public void ChangeRightLook()
    {
        characterIsLookingRight[moveCharacterDd.value] = isMovingLeft.isOn;
    }
    public void ChangeAnimationStartWait()
    {
        animateStartTimes[whotoAnimateDd.value] = float.Parse(secondaryAnimationWaitTime.text); 
    }
    public void ChangeAnimationToggle()
    {
        isAnimated[whotoAnimateDd.value] = isAnimatedToggle.isOn;
    }
    public void ChangeAnimationLoopToggle()
    {
        loopAnimation[dialWhoToAnimateDd.value] = dialIsLoopedToggle.isOn;
    }
    public void ChangeDiagAnimationCounter()
    {
        animationCounters[dialWhoToAnimateDd.value] = dialWhatAnimationDd.value;
    }
    public void ChangeAnimationCounter()
    {
        animationCounters[whotoAnimateDd.value] = whatAnimationDd.value;
    }
    public void MoveDelaySet()
    {
        moveDelays[moveCharacterDd.value] = float.Parse(moveDelay.text);
    }
    public void ChangeSetupCharacter()
    {
        selectedCharacterXpos.text = "" + charactersInStorySpawnPosition[selectedCharacterDd.value].x;
        selectedCharacterYpos.text = "" + charactersInStorySpawnPosition[selectedCharacterDd.value].y;
        setupLookToggle.isOn = characterIsLookingRight[selectedCharacterDd.value];
    }
    public void ChangeDiagAnimationToggle()
    {
        isAnimated[dialWhoToAnimateDd.value] = dialIsAnimatedToggle.isOn;
    }
    public void ChangeDiagAnimationDelay()
    {
        moveDelays[dialWhoToAnimateDd.value] = float.Parse(dialAnimationDelay.text);
    }
    public void ChangeSetupX()
    {
        charactersInStorySpawnPosition[selectedCharacterDd.value].x = float.Parse(selectedCharacterXpos.text);
    }
    public void ChangeCharacterLayer()
    {
        characterLayer[selectedCharacterDd.value] = zLayer.value;
    }
    public void ChangeMoveX()
    {
        characterMovePos[moveCharacterDd.value].x = float.Parse(moveToPosX.text);
    }
    public void ChangeMoveY()
    {
        characterMovePos[moveCharacterDd.value].y = float.Parse(moveToPosY.text);
    }
    public void ChangeSetupY()
    {
        charactersInStorySpawnPosition[selectedCharacterDd.value].y = float.Parse(selectedCharacterYpos.text);
    }
    public void MoveToggleSwitch()
    {
        characterIsMoved[moveCharacterDd.value] = moveThisToggle.isOn;
    }
    public void ChangeMoveCharacter()
    {
        moveThisToggle.isOn = characterIsMoved[moveCharacterDd.value];
        moveDelay.text = "" + moveDelays[moveCharacterDd.value];
        moveToPosX.text = "" + characterMovePos[moveCharacterDd.value].x;
        moveToPosY.text = "" + characterMovePos[moveCharacterDd.value].y;
    }
    public void ChangeStory()
    {
        storyName = allStoriesDd.options[allStoriesDd.value].text;
        GetSceneList();
        storyNameText.text = storyName;
        jsl.CheckHidden(storyName);
    }

    public void SelectBranch(string branch)
    {
        for (int i = 0; i < branches.Length; i++)
        {
            if (branch == branches[i])
            {
                currentBranch = i;
                currentBranchScene = 0;
            }
        }
    }
    public void SelectBranchScene(int id)
    {
        currentBranchScene = id;
    }
    #endregion

    public void AddCharacterToList()
    {
        string s = allCharactersDd.options[allCharactersDd.value].text;
        string[] sa = new string[] { };
        bool[] bo = new bool[] { };
        bool[] b2 = new bool[] { };
        int[] cl = new int[] { };

        for(int i = 0; i < charactersInStory.Length; i++)
        {
            if(charactersInStory[i] == s)
            {
                return;
            }
        }      
        if (charactersInStory.Length > 0)
        {
            sa = new string[charactersInStory.Length + 1];
            bo = new bool[charactersInStory.Length + 1];
            b2 = new bool[charactersInStory.Length + 1];
            cl = new int[charactersInStory.Length + 1];
            for (int y = 0; y < charactersInStory.Length; y++)
            {
                sa[y] = charactersInStory[y];
                bo[y] = characterIsMoved[y];
                b2[y] = characterIsLookingRight[y];
            }
            sa[sa.Length - 1] = s;
            bo[bo.Length - 1] = false;
            b2[b2.Length - 1] = false;
            cl[cl.Length - 1] = 0;
            charactersInStory = new string[sa.Length];
            charactersInStory = sa;
            Vector2[] v = new Vector2[sa.Length];
            for(int xy = 0; xy < sa.Length-1; xy++)
            {
                v[xy] = charactersInStorySpawnPosition[xy];
            }
            v[sa.Length-1].x = 0;
            v[sa.Length-1].y = 0;
            charactersInStorySpawnPosition = v;
            characterIsMoved = bo;
            characterIsLookingRight = b2;
            characterLayer = cl;
        }
        if (charactersInStory.Length == 0)
        {
            sa = new string[1];
            bo = new bool[1];
            b2 = new bool[1];
            cl = new int[1];
            sa[0] = s;
            charactersInStory = new string[1];
            charactersInStory = sa;
            Vector2[] v = new Vector2[1];
            v[0].x = 0;
            v[0].y = 0;
            charactersInStorySpawnPosition = v;
            characterIsMoved = bo;
            characterIsLookingRight = b2;
            characterLayer = cl;
        }
        
        selectedCharacterDd.ClearOptions();
        List<string> li = new List<string>();
        for(int x = 0; x < sa.Length; x++)
        {
            li.Add(sa[x]);
        }
        selectedCharacterDd.AddOptions(li);
        
        selectedCharacterXpos.text = "" + charactersInStorySpawnPosition[selectedCharacterDd.value].x;
        selectedCharacterYpos.text = "" + charactersInStorySpawnPosition[selectedCharacterDd.value].y;
    }
   
    public void RemoveCharacterFromList()
    {
        List<string> li = new List<string>();
        List<bool> bl = new List<bool>();
        List<bool> b2 = new List<bool>();
        List<int> cl = new List<int>();
        List<Vector2> vl = new List<Vector2>();
        for(int i = 0; i < charactersInStory.Length; i++)
        {
            if(charactersInStory[i] != selectedCharacterDd.options[selectedCharacterDd.value].text)
            {
                li.Add(charactersInStory[i]);
                vl.Add(charactersInStorySpawnPosition[i]);
                bl.Add(characterIsMoved[i]);
                b2.Add(characterIsLookingRight[i]);
                cl.Add(characterLayer[i]);
                Debug.Log(charactersInStory[i]);
            }
        }
        selectedCharacterDd.ClearOptions();
        selectedCharacterDd.AddOptions(li);
        charactersInStory = new string[li.Count];
        charactersInStorySpawnPosition = new Vector2[li.Count];
        characterIsMoved = new bool[li.Count];
        characterIsLookingRight = new bool[li.Count];
        characterLayer = new int[li.Count];
        for (int x = 0; x < li.Count; x++)
        {
            charactersInStory[x] = li[x];
            charactersInStorySpawnPosition[x] = vl[x];
            characterIsMoved[x] = bl[x];
            characterIsLookingRight[x] = b2[x];
            characterLayer[x] = cl[x];
        }
        selectedCharacterDd.value = 0;
        setupLookToggle.isOn = false;
        if(charactersInStory.Length > 0)
        {
            selectedCharacterXpos.text = "" + charactersInStorySpawnPosition[selectedCharacterDd.value].x;
            selectedCharacterYpos.text = "" + charactersInStorySpawnPosition[selectedCharacterDd.value].y;
        }
        else
        {
            selectedCharacterXpos.text = "0";
            selectedCharacterYpos.text = "0";
        }
    }
    void GetAllUIElements()
    {
        
        mainCanvas = GameObject.Find("MainCanvas").GetComponent<Canvas>();
        newStoryInput = GameObject.Find("NewStoryInputField").GetComponent<InputField>();
        passwordInput = GameObject.Find("PasswordInputField").GetComponent<InputField>();
        hiddenToggle = GameObject.Find("HiddenToggle").GetComponent<Toggle>();
       
        //global for all scenes
        allScenesDd = GameObject.Find("AllScenesDD").GetComponent<Dropdown>();
        storyNameText = GameObject.Find("StoryName").GetComponent<Text>();
        sceneTypeDd = GameObject.Find("SceneTypeDD").GetComponent<Dropdown>();
        thisBranchDd = GameObject.Find("ThisBranchDD").GetComponent<Dropdown>();
        thisSceneDd = GameObject.Find("ThisBranchNumberDD").GetComponent<Dropdown>();
        nextBranchDd = GameObject.Find("NextBranchDD").GetComponent<Dropdown>();
        nextSceneDd = GameObject.Find("NextBranchNumberDD").GetComponent<Dropdown>();
        secondaryBranchDd = GameObject.Find("SecondaryBranchDD").GetComponent<Dropdown>();
        secondarySceneDd = GameObject.Find("SecondaryBranchNumberDD").GetComponent<Dropdown>();
        tetriaryBranchDd = GameObject.Find("TetriaryBranchDD").GetComponent<Dropdown>();
        tetriarySceneDd = GameObject.Find("TetriaryBranchNumberDD").GetComponent<Dropdown>();

        List<string> l1 = new List<string>() { };
        for (int i = 0; i < 100; i++)
        {
            l1.Add(i.ToString());
        }
        
        thisSceneDd.ClearOptions();
        nextSceneDd.ClearOptions();
        secondarySceneDd.ClearOptions();
        tetriarySceneDd.ClearOptions();
        thisSceneDd.AddOptions(l1);
        nextSceneDd.AddOptions(l1);
        secondarySceneDd.AddOptions(l1);
        tetriarySceneDd.AddOptions(l1);
        thisBranchDd.ClearOptions();
        nextBranchDd.ClearOptions();
        secondaryBranchDd.ClearOptions();
        tetriaryBranchDd.ClearOptions();
        thisBranchDd.AddOptions(alphabets);
        nextBranchDd.AddOptions(alphabets);
        secondaryBranchDd.AddOptions(alphabets);
        tetriaryBranchDd.AddOptions(alphabets);
        //dialogue
        dialogueInput = GameObject.Find("DialogueInput").GetComponent<InputField>();
        dialogueSpeaker = GameObject.Find("DialogueNameInput").GetComponent<InputField>();
        dialogueNarrator = GameObject.Find("DialogueTellerToggle").GetComponent<Toggle>();
        //selection
        selectionInput = GameObject.Find("QuestionInput").GetComponent<InputField>();
        selectionOption1 = GameObject.Find("Selection1Input").GetComponent<InputField>();
        selectionOption2 = GameObject.Find("Selection2Input").GetComponent<InputField>();
        selectionOption3 = GameObject.Find("Selection3Input").GetComponent<InputField>();
        //setup
        backgroundSetupDd = GameObject.Find("BackgroundDD").GetComponent<Dropdown>();
        allCharactersDd = GameObject.Find("AllCharactersDD").GetComponent<Dropdown>();
        selectedCharacterDd = GameObject.Find("SelectedCharacterDD").GetComponent<Dropdown>();
        selectedCharacterXpos = GameObject.Find("StartXInput").GetComponent<InputField>();
        selectedCharacterYpos = GameObject.Find("StartYInput").GetComponent<InputField>();
        setupToggle = GameObject.Find("SetupToggle").GetComponent<Toggle>();
        setupLookToggle = GameObject.Find("SetupLookToggle").GetComponent<Toggle>();
        setupWaitTime = GameObject.Find("SetupWaitInput").GetComponent<InputField>();
        zLayer = GameObject.Find("FrontBackDd").GetComponent<Dropdown>();
        List<string> bgli = new List<string>() { };
        for(int i = 0; i < slib.backgroundSprites.Length; i++)
        {
            bgli.Add(slib.backgroundSprites[i].name);
        }
        backgroundSetupDd.ClearOptions();
        backgroundSetupDd.AddOptions(bgli);
       
        //move
        moveCharacterDd = GameObject.Find("MoveCharacterDD").GetComponent<Dropdown>();
        moveThisToggle = GameObject.Find("MoveToggle").GetComponent<Toggle>();
        isMovingLeft = GameObject.Find("MoveLeftToggle").GetComponent<Toggle>();
        moveToPosX = GameObject.Find("MoveToX").GetComponent<InputField>();
        moveToPosY = GameObject.Find("MoveToY").GetComponent<InputField>();
        movementSpeed = GameObject.Find("MoveSpeedInput").GetComponent<InputField>();
        moveDelay = GameObject.Find("MoveDelayInput").GetComponent<InputField>();
        moveWait = GameObject.Find("MoveWaitInput").GetComponent<InputField>();
        //end
        endWait = GameObject.Find("EndWaitInput").GetComponent<InputField>();
        //cutscene
        cutsceneDd = GameObject.Find("CutsceneDD").GetComponent<Dropdown>();
        cutsceneWait = GameObject.Find("CutsceneWait").GetComponent<InputField>();
        //fade
        fadeInToggle = GameObject.Find("FadeToggle").GetComponent<Toggle>();
        fadeWaitInput = GameObject.Find("FadeWaitInput").GetComponent<InputField>();
        //zoom
        zoomReset = GameObject.Find("ZoomResetToggle").GetComponent<Toggle>();
        zoomSpeed = GameObject.Find("ZoomSpeedInput").GetComponent<InputField>();
        zoomAmount = GameObject.Find("ZoomInput").GetComponent<InputField>();
        zoomXPos = GameObject.Find("ZoomXInput").GetComponent<InputField>();
        zoomYPos = GameObject.Find("ZoomYInput").GetComponent<InputField>();
        zoomWait = GameObject.Find("ZoomWaitInput").GetComponent<InputField>();
        //animation
        whotoAnimateDd = GameObject.Find("AnimationCharacterDd").GetComponent<Dropdown>();
        isAnimatedToggle = GameObject.Find("AnimationCharacterToggle").GetComponent<Toggle>();
        whatAnimationDd = GameObject.Find("AnimationListDD").GetComponent<Dropdown>();
        isLoopedAnimationToggle = GameObject.Find("AnimationLoopToggle").GetComponent<Toggle>();
        secondaryAnimationWaitTime = GameObject.Find("AnimationStartWaitInput").GetComponent<InputField>();
        animationWaitTime = GameObject.Find("AnimationWaitInput").GetComponent<InputField>();
        //dialogueanimation
        dialWhoToAnimateDd = GameObject.Find("DialWhoDD").GetComponent<Dropdown>();
        dialIsAnimatedToggle = GameObject.Find("DialIsAnimatedToggle").GetComponent<Toggle>();
        dialWhatAnimationDd = GameObject.Find("DialAnimationDD").GetComponent<Dropdown>();
        dialIsLoopedToggle = GameObject.Find("DialLoopToggle").GetComponent<Toggle>();
        dialIsNarrator = GameObject.Find("DialTellerToggle").GetComponent<Toggle>();
        dialName = GameObject.Find("DialNameInput").GetComponent<InputField>();
        dialDialogue = GameObject.Find("DialDialogueInput").GetComponent<InputField>();
        dialAnimationDelay = GameObject.Find("DialDelayInput").GetComponent<InputField>();
        //change clothes

        //playsound
        soundList = GameObject.Find("SoundDD").GetComponent<Dropdown>();
        soundWait = GameObject.Find("SoundWait").GetComponent<InputField>();
        //textmessage
        receivingToggle = GameObject.Find("ReceivingToggle").GetComponent<Toggle>();
        textmessageInput = GameObject.Find("MessageInput").GetComponent<InputField>();
        textmessageNameInput = GameObject.Find("MessageNameInput").GetComponent<InputField>();
        //tooltip
        tooltipmessageInput = GameObject.Find("PopUpMessageInput").GetComponent<InputField>();
        //panels
        editorPanels = new GameObject[14];
        editorPanels[0] = GameObject.Find("SetupPanel");
        editorPanels[1] = GameObject.Find("FadePanel");
        editorPanels[2] = GameObject.Find("DialoguePanel");
        editorPanels[3] = GameObject.Find("DialogueAnimationPanel");
        editorPanels[4] = GameObject.Find("SelectionPanel");
        editorPanels[5] = GameObject.Find("MovePanel");
        editorPanels[6] = GameObject.Find("PlaySound");
        editorPanels[7] = GameObject.Find("CutscenePanel");
        editorPanels[8] = GameObject.Find("ChangeClothePanel");
        editorPanels[9] = GameObject.Find("AnimationPanel");
        editorPanels[10] = GameObject.Find("ZoomPanel");
        editorPanels[11] = GameObject.Find("EndGamePanel");
        editorPanels[12] = GameObject.Find("TextMessagePanel");
        editorPanels[13] = GameObject.Find("PopUpPanel");
        for (int i = 0; i < editorPanels.Length; i++)
        {
            editorPanels[i].SetActive(editorPanels[i].name == "SetupPanel");
        }
    }
	IEnumerator StartFade()
    {
        //starts the fade in out function with delay
        yield return new WaitForSeconds(1.0f);
        fio.FadeIn();
    }

    public void HideShowMain(bool b)
    {
        
        if (b)
        {
            mainCanvas.GetComponent<CanvasGroup>().alpha = 1;
            mainCanvas.GetComponent<CanvasGroup>().interactable = b;
            mainCanvas.GetComponent<CanvasGroup>().blocksRaycasts = b;
        }
        if (!b)
        {
            mainCanvas.GetComponent<CanvasGroup>().alpha = 0;
            mainCanvas.GetComponent<CanvasGroup>().interactable = b;
            mainCanvas.GetComponent<CanvasGroup>().blocksRaycasts = b;
        }
        jsl.SetHidden(hiddenList[allStoriesDd.value]);
    }
    public void SelectSpecificSceneType(int type)
    {
        //when you change the scene type
        int selectedSceneType = 0;
        sceneTypeDd.value = type;
        selectedSceneType = type;
        sceneType = selectedSceneType;
        for (int i = 0; i < editorPanels.Length; i++)
        {
            editorPanels[i].SetActive(selectedSceneType == i);
        }

        if (sceneType == 4)
        {
            secondaryBranchDd.interactable = true;
            secondarySceneDd.interactable = true;
        }
        if (sceneType != 4)
        {
            secondaryBranchDd.interactable = false;
            secondarySceneDd.interactable = false;
        }
        if (sceneType != 11)
        {
            nextBranchDd.interactable = true;
            nextSceneDd.interactable = true;
        }
        if (sceneType == 5)
        {
            List<string> chrs = new List<string>();
            for (int x = 0; x < charactersInStory.Length; x++)
            {
                chrs.Add(charactersInStory[x]);
            }
            moveCharacterDd.ClearOptions();
            moveCharacterDd.AddOptions(chrs);
            moveCharacterDd.value = 0;
        }
        if (sceneType == 3 || sceneType == 9)
        {
            List<string> chrs = new List<string>();
            for (int x = 0; x < charactersInStory.Length; x++)
            {
                chrs.Add(charactersInStory[x]);
            }
            if (sceneType == 3)
            {
                dialWhoToAnimateDd.ClearOptions();
                dialWhoToAnimateDd.AddOptions(chrs);
                dialWhoToAnimateDd.value = 0;
            }
            if (sceneType == 9)
            {
                whotoAnimateDd.ClearOptions();
                whotoAnimateDd.AddOptions(chrs);
                whotoAnimateDd.value = 0;
                animateStartTimes = new float[charactersInStory.Length];
            }
            if (sceneType == 11)
            {
                nextBranchDd.interactable = false;
                nextSceneDd.interactable = false;
            }
            animationCounters = new int[charactersInStory.Length];
            isAnimated = new bool[charactersInStory.Length];
            loopAnimation = new bool[charactersInStory.Length];
        }
    }
    public void SelectSceneType()
    {
        int selectedSceneType = 0;
        selectedSceneType = sceneTypeDd.value;
        sceneType = selectedSceneType;
        for (int i = 0; i < editorPanels.Length; i++)
        {
            editorPanels[i].SetActive(selectedSceneType == i);
        }

        if (sceneType == 4)
        {
            secondaryBranchDd.interactable = true;
            secondarySceneDd.interactable = true;
            tetriarySceneDd.interactable = true;
            tetriaryBranchDd.interactable = true;
        }
        if (sceneType != 4)
        {
            secondaryBranchDd.interactable = false;
            secondarySceneDd.interactable = false;
            tetriaryBranchDd.interactable = false;
            tetriarySceneDd.interactable = false;
        }
        if (sceneType != 11)
        {
            nextBranchDd.interactable = true;
            nextSceneDd.interactable = true;
        }
        if (sceneType == 5)
        {
            List<string> chrs = new List<string>();
            for (int x = 0; x < charactersInStory.Length; x++)
            {
                chrs.Add(charactersInStory[x]);
            }
            characterIsMoved = new bool[chrs.Count];
            moveDelays = new float[chrs.Count];
            characterMovePos = new Vector2[chrs.Count];
            moveCharacterDd.ClearOptions();
            moveCharacterDd.AddOptions(chrs);
            moveCharacterDd.value = 0;
        }
        if (sceneType == 3 || sceneType == 9)
        {
            List<string> chrs = new List<string>();
            for (int x = 0; x < charactersInStory.Length; x++)
            {
                chrs.Add(charactersInStory[x]);
            }
            if (sceneType == 3)
            {
                moveDelays = new float[chrs.Count];
                isAnimated = new bool[chrs.Count];
                animationCounters = new int[chrs.Count];
                dialWhoToAnimateDd.ClearOptions();
                dialWhoToAnimateDd.AddOptions(chrs);
                dialWhoToAnimateDd.value = 0;
            }
            if (sceneType == 9)
            {
                whotoAnimateDd.ClearOptions();
                whotoAnimateDd.AddOptions(chrs);
                whotoAnimateDd.value = 0;
            }
            animationCounters = new int[charactersInStory.Length];
            isAnimated = new bool[charactersInStory.Length];
            loopAnimation = new bool[charactersInStory.Length];
            animateStartTimes = new float[charactersInStory.Length];

        }
        if(sceneType == 11)
        {
            nextBranchDd.interactable = false;
            nextSceneDd.interactable = false;
        }
     
    }
    public void SaveNewStory()
    {
        //saves the new created story template
        bool b = false;
        for(int i = 0; i < storyCount; i++)
        {
            if(newStoryInput.text == allStoriesDd.options[i].text)
            {
                Debug.Log(newStoryInput.text + " / " + allStoriesDd.options[i].text);
                b = true;
            }
        }
        
        if(newStoryInput.text != "" && newStoryInput.text != " " && newStoryInput.text != "  " && newStoryInput.text != "   " && b == false)
        {
            jsl.SaveStoryToJson(newStoryInput.text, hiddenToggle.isOn, passwordInput.text);
        }
        GetStuff();
    }


    public void CreateScene()
    {
        //creates scene
        switch(sceneType)
        {
            case 0: //setup
                scl = new ScenarioClass();
                scl.scenarioType = 4;
                scl.thisScenario = "" + thisBranchDd.options[thisBranchDd.value].text + thisSceneDd.options[thisSceneDd.value].text;
                scl.nextScenario = "" + nextBranchDd.options[nextBranchDd.value].text + nextSceneDd.options[nextSceneDd.value].text;
                scl.lookingRight = characterIsLookingRight;
                scl.backgroundCounter = backgroundSetupDd.value;
                scl.characterToSpawn = charactersInStory;
                scl.startPositions = charactersInStorySpawnPosition;
                scl.waitTime = float.Parse(setupWaitTime.text);
                scl.fadeIn = setupToggle.isOn;
                scl.layerCounter = characterLayer;
                break;
            case 1: //fade
                scl = new ScenarioClass();
                scl.thisScenario = "" + thisBranchDd.options[thisBranchDd.value].text + thisSceneDd.options[thisSceneDd.value].text;
                scl.nextScenario = "" + nextBranchDd.options[nextBranchDd.value].text + nextSceneDd.options[nextSceneDd.value].text;
                scl.scenarioType = 0;
                scl.fadeIn = fadeInToggle.isOn;
                scl.waitTime = float.Parse(fadeWaitInput.text);
                Debug.Log(scl.thisScenario + " " + scl.nextScenario + " " + scl.scenarioType + " " + scl.fadeIn + " " + scl.waitTime);
                break;
            case 2: //dialogye
                scl = new ScenarioClass();
                scl.thisScenario = "" + thisBranchDd.options[thisBranchDd.value].text + thisSceneDd.options[thisSceneDd.value].text;
                scl.nextScenario = "" + nextBranchDd.options[nextBranchDd.value].text + nextSceneDd.options[nextSceneDd.value].text;
                scl.scenarioType = 1;
                scl.characterName = "" + dialogueSpeaker.text;
                scl.dialogue = "" + dialogueInput.text;
                scl.isTeller = dialogueNarrator.isOn;
                break;
            case 3: // diag anim
                scl = new ScenarioClass();
                scl.isTeller = dialIsNarrator.isOn;
                scl.characterName = "" + dialName.text;
                scl.dialogue = "" + dialDialogue.text;
                scl.charactersInScene = charactersInStory;
                scl.scenarioType = 3;
                scl.thisScenario = "" + thisBranchDd.options[thisBranchDd.value].text + thisSceneDd.options[thisSceneDd.value].text;
                scl.nextScenario = "" + nextBranchDd.options[nextBranchDd.value].text + nextSceneDd.options[nextSceneDd.value].text;
                scl.loopAnimation = loopAnimation;
                scl.isAnimated = isAnimated;
                scl.animationId = animationCounters;
                scl.secondaryWaitTime = moveDelays;
                break;
            case 4://selection
                scl = new ScenarioClass();
                scl.thisScenario = "" + thisBranchDd.options[thisBranchDd.value].text + thisSceneDd.options[thisSceneDd.value].text;
                scl.nextScenario = "";
                scl.scenarioType = 2;
                scl.selectionQuestion = "" + selectionInput.text;
                scl.selectionOption1 = "" + selectionOption1.text;
                scl.selectionOption2 = "" + selectionOption2.text;
                scl.selectionOption3 = "" + selectionOption3.text;
                scl.isTeller = true;
                scl.nextBranch1 = "" + nextBranchDd.options[nextBranchDd.value].text + nextSceneDd.options[nextSceneDd.value].text;
                scl.nextBranch2 = "" + secondaryBranchDd.options[secondaryBranchDd.value].text + secondarySceneDd.options[secondarySceneDd.value].text;
                scl.nextBranch3 = "" + tetriaryBranchDd.options[tetriaryBranchDd.value].text + tetriarySceneDd.options[tetriarySceneDd.value].text;
                break;
            case 5://move character
                scl = new ScenarioClass();
                scl.scenarioType = 5;
                scl.thisScenario = "" + thisBranchDd.options[thisBranchDd.value].text + thisSceneDd.options[thisSceneDd.value].text;
                scl.nextScenario = "" + nextBranchDd.options[nextBranchDd.value].text + nextSceneDd.options[nextSceneDd.value].text;
                scl.charactersInScene = charactersInStory;
                scl.moveThis = characterIsMoved;
                scl.lookingRight = characterIsLookingRight;
                scl.moveTo = characterMovePos;
                scl.secondaryWaitTime = moveDelays;
                scl.waitTime = float.Parse(moveWait.text);
                scl.moveSpeed = float.Parse(movementSpeed.text);
                break;
            case 6://playsound
                scl = new ScenarioClass();
                scl.scenarioType = 6;
                scl.thisScenario = "" + thisBranchDd.options[thisBranchDd.value].text + thisSceneDd.options[thisSceneDd.value].text;
                scl.nextScenario = "" + nextBranchDd.options[nextBranchDd.value].text + nextSceneDd.options[nextSceneDd.value].text;
                scl.waitTime = float.Parse(soundWait.text);
                scl.audioClip = soundList.value;
                break;
            case 7: //cutscene
                scl = new ScenarioClass();
                scl.scenarioType = 7;
                scl.thisScenario = "" + thisBranchDd.options[thisBranchDd.value].text + thisSceneDd.options[thisSceneDd.value].text;
                scl.nextScenario = "" + nextBranchDd.options[nextBranchDd.value].text + nextSceneDd.options[nextSceneDd.value].text;
                scl.waitTime = float.Parse(cutsceneWait.text);
                scl.selectedCutscene = cutsceneDd.value;
                break;
            case 8://change cloth
                scl = new ScenarioClass();
                scl.scenarioType = 8;
                scl.thisScenario = "" + thisBranchDd.options[thisBranchDd.value].text + thisSceneDd.options[thisSceneDd.value].text;
                scl.nextScenario = "" + nextBranchDd.options[nextBranchDd.value].text + nextSceneDd.options[nextSceneDd.value].text;

                scl.waitTime = float.Parse(""); //NOTREADY
                scl.clotheid = int.Parse(""); //NOTREADY
                scl.subid = int.Parse(""); //NOTREADY
                break;
            case 9://animaatio
                scl = new ScenarioClass();
                scl.scenarioType = 11;
                scl.thisScenario = "" + thisBranchDd.options[thisBranchDd.value].text + thisSceneDd.options[thisSceneDd.value].text;
                scl.nextScenario = "" + nextBranchDd.options[nextBranchDd.value].text + nextSceneDd.options[nextSceneDd.value].text;
                scl.waitTime = float.Parse(animationWaitTime.text);
                scl.charactersInScene = charactersInStory;
                scl.secondaryWaitTime = animateStartTimes;
                scl.isAnimated = isAnimated;
                scl.animationId = animationCounters;
                scl.loopAnimation = loopAnimation;
                break;
            case 10://zoom
                scl = new ScenarioClass();
                scl.scenarioType = 10;
                scl.thisScenario = "" + thisBranchDd.options[thisBranchDd.value].text + thisSceneDd.options[thisSceneDd.value].text;
                scl.nextScenario = "" + nextBranchDd.options[nextBranchDd.value].text + nextSceneDd.options[nextSceneDd.value].text;
                scl.zoomAmount = float.Parse(zoomAmount.text);
                scl.reset = zoomReset.isOn;
                scl.waitTime = float.Parse(zoomWait.text);
                scl.zoomSpeed = float.Parse(zoomSpeed.text);
                scl.zoomPosition = new Vector2(float.Parse(zoomXPos.text), float.Parse(zoomYPos.text));
                break;
            case 11://end
                scl = new ScenarioClass();
                scl.thisScenario = "" + thisBranchDd.options[thisBranchDd.value].text + thisSceneDd.options[thisSceneDd.value].text;
                scl.scenarioType = 9;
                scl.waitTime = float.Parse(endWait.text);
                scl.isEnd = true;
                break;
            case 12://text message
                scl = new ScenarioClass();
                scl.thisScenario = "" + thisBranchDd.options[thisBranchDd.value].text + thisSceneDd.options[thisSceneDd.value].text;
                Debug.Log("¤" + scl.thisScenario);
                scl.nextScenario = "" + nextBranchDd.options[nextBranchDd.value].text + nextSceneDd.options[nextSceneDd.value].text;
                scl.scenarioType = 13;
                scl.dialogue = textmessageInput.text;
                scl.characterName = textmessageNameInput.text;
                scl.isEnd = receivingToggle.isOn;
                break;
            case 13://tooltip
                scl = new ScenarioClass();
                scl.thisScenario = "" + thisBranchDd.options[thisBranchDd.value].text + thisSceneDd.options[thisSceneDd.value].text;
                scl.nextScenario = "" + nextBranchDd.options[nextBranchDd.value].text + nextSceneDd.options[nextSceneDd.value].text;
                scl.scenarioType = 14;
                scl.dialogue = "" + tooltipmessageInput.text;
                break;
                
            
        }
        //endcreate

        //save
        Debug.Log(allStoriesDd.options[allStoriesDd.value].text);
        jsl.SaveSceneToJson(scl, allStoriesDd.options[allStoriesDd.value].text);
        
        GetSceneList();
        Debug.Log("Scene Saved");
    }
    public void GetSceneList()
    {
        //gets the list of all scenes
        List<string> slist = new List<string>() { };
        debtxt.text = "10.1";
        allScenesDd.ClearOptions();
        string[] sar = new string[0];
        try
        {
            debtxt.text = "10.2";
            sar = jsl.LoadSceneList(allStoriesDd.options[allStoriesDd.value].text);
            debtxt.text = "10.3";
            for (int i = 0; i < sar.Length; i++)
            {
                slist.Add(sar[i]);
            }
            debtxt.text = "10.4";
            allScenesDd.AddOptions(slist);
        }
        catch
        {
            sar = new string[0];
        }
        debtxt.text = "10.5";


    }
    public void LoadScene()
    {
        //loads the selected scene
        ScenarioClass c = jsl.GetScenario(allStoriesDd.options[allStoriesDd.value].text, allScenesDd.options[allScenesDd.value].text);
        for(int v1 = 0; v1 < thisBranchDd.options.Count; v1++)
        {
            if (thisBranchDd.options[v1].text == c.thisScenario[0].ToString())
            {
                thisBranchDd.value = v1;
            }
        }
        for(int v2 = 0; v2 < thisSceneDd.options.Count; v2++)
        {
            string prs = "" + c.thisScenario;
            char[] chrs = prs.ToCharArray();
            prs = "";
            for (int i = 0; i < chrs.Length; i++)
            {
                if (i != 0)
                {
                    prs += chrs[i];
                }
            }
            int ev = int.Parse(prs);
            thisSceneDd.value = ev;
        }
        for(int v3 = 0; v3 < nextBranchDd.options.Count; v3++)
        {
            if (!c.isEnd)
            {
                if(c.scenarioType != 2)
                {
                    if (nextBranchDd.options[v3].text == c.nextScenario[0].ToString())
                    {
                        nextBranchDd.value = v3;
                    }
                }
                if (c.scenarioType == 2)
                {
                    if (nextBranchDd.options[v3].text == c.nextBranch1[0].ToString())
                    {
                        nextBranchDd.value = v3;
                    }
                    if (secondaryBranchDd.options[v3].text == c.nextBranch2[0].ToString())
                    {
                        secondaryBranchDd.value = v3;
                    }
                    if(tetriaryBranchDd.options[v3].text == c.nextBranch3[0].ToString())
                    {
                        tetriaryBranchDd.value = v3;
                    }
                }
            }
        }
        for(int v4 = 0; v4 < nextSceneDd.options.Count; v4++)
        {
            if(!c.isEnd)
            {
                if(c.scenarioType != 2)
                {
                    string prs = "" + c.nextScenario;
                    char[] chrs = prs.ToCharArray();
                    prs = "";
                    for (int i = 0; i < chrs.Length; i++)
                    {
                        if (i != 0)
                        {
                            prs += chrs[i];
                        }
                    }
                    int ev = int.Parse(prs);
                    nextSceneDd.value = ev;
                }
                if(c.scenarioType == 2)
                {
                    string prs = "" + c.nextBranch1;
                    char[] chrs = prs.ToCharArray();
                    prs = "";
                    for (int i = 0; i < chrs.Length; i++)
                    {
                        if (i != 0)
                        {
                            prs += chrs[i];
                        }
                    }
                    int ev = int.Parse(prs);
                    nextSceneDd.value = ev;

                    string prs1 = "" + c.nextBranch2;
                    char[] chrs1 = prs1.ToCharArray();
                    prs1 = "";
                    for (int i = 0; i < chrs1.Length; i++)
                    {
                        if (i != 0)
                        {
                            prs1 += chrs1[i];
                        }
                    }
                    int ev1 = int.Parse(prs1);
                    secondarySceneDd.value = ev1;

                    string prs2 = "" + c.nextBranch2;
                    char[] chrs2 = prs2.ToCharArray();
                    prs2 = "";
                    for (int i = 0; i < chrs2.Length; i++)
                    {
                        if (i != 0)
                        {
                            prs2 += chrs2[i];
                        }
                    }
                    int ev2 = int.Parse(prs2);
                    secondarySceneDd.value = ev2;
                }
            }
        }
        switch (c.scenarioType)
        {
            case 0:
                SelectSpecificSceneType(1);
                fadeInToggle.isOn = c.fadeIn;
                fadeWaitInput.text = "" + c.waitTime;
                break;
            case 1:
                SelectSpecificSceneType(2);
                dialogueNarrator.isOn = c.isTeller;
                dialogueSpeaker.text = c.characterName;
                dialogueInput.text = c.dialogue;
                break;
            case 2:
                SelectSpecificSceneType(4);
                selectionInput.text = c.selectionQuestion;
                selectionOption1.text = c.selectionOption1;
                selectionOption2.text = c.selectionOption2;
                selectionOption3.text = c.selectionOption3;
                break;
            case 3:
                SelectSpecificSceneType(3);
                dialWhoToAnimateDd.value = 0;
                dialWhoToAnimateDd.ClearOptions();
                charactersInStory = c.charactersInScene;
                List<string> whs = new List<string>() { };
                for(int wh = 0; wh < charactersInStory.Length; wh++)
                {
                    whs.Add(charactersInStory[wh]);
                }
                dialWhoToAnimateDd.AddOptions(whs);
                isAnimated = c.isAnimated;
                dialIsAnimatedToggle.isOn = isAnimated[0];
                dialWhatAnimationDd.value = c.animationId[0];
                loopAnimation = c.loopAnimation;
                dialIsLoopedToggle.isOn = loopAnimation[0];
                dialDialogue.text = c.dialogue;
                dialName.text = c.characterName;
                moveDelays = c.secondaryWaitTime;
                dialAnimationDelay.text = "" + moveDelays[0];
                break;
            case 4:
                SelectSpecificSceneType(0);
                charactersInStory = c.characterToSpawn;
                selectedCharacterDd.value = 0;
                List<string> sst = new List<string> { };
                for(int c4 = 0; c4 < charactersInStory.Length; c4++)
                {
                    sst.Add(charactersInStory[c4]);
                }
                selectedCharacterDd.ClearOptions();
                selectedCharacterDd.AddOptions(sst);
                charactersInStorySpawnPosition = c.startPositions;
                if(charactersInStorySpawnPosition.Length > 0)
                {
                    selectedCharacterXpos.text = "" + charactersInStorySpawnPosition[0].x;
                    selectedCharacterYpos.text = "" + charactersInStorySpawnPosition[0].y;
                }      
                backgroundSetupDd.value = c.backgroundCounter;
                setupToggle.isOn = c.fadeIn;
                characterIsLookingRight = c.lookingRight;
                characterLayer = c.layerCounter;
                setupWaitTime.text = "" + c.waitTime;
                break;
            case 5:
                SelectSpecificSceneType(5);
                charactersInStory = c.charactersInScene;
                moveCharacterDd.value = 0;
                moveCharacterDd.ClearOptions();
                List<string> l5 = new List<string>() { };
                for(int c5 = 0; c5 < charactersInStory.Length; c5++)
                {
                    l5.Add(charactersInStory[c5]);
                }
                moveCharacterDd.AddOptions(l5);
                moveDelays = c.secondaryWaitTime;
                moveDelay.text = "" + moveDelays[0];
                characterIsLookingRight = c.lookingRight;
                characterIsMoved = c.moveThis;
                characterLayer = c.layerCounter;
                isMovingLeft.isOn = characterIsLookingRight[0];
                break;
            case 6:
                SelectSpecificSceneType(6);
                break;
            case 7:
                SelectSpecificSceneType(7);
                cutsceneDd.value = c.selectedCutscene;
                cutsceneWait.text = "" + c.waitTime;
                break;
            case 8:
                SelectSpecificSceneType(8);
                break;
            case 9:
                SelectSpecificSceneType(11);
                endWait.text = "" + c.waitTime;
                break;
            case 10:
                SelectSpecificSceneType(10);
                zoomReset.isOn = c.reset;
                zoomSpeed.text = "" + c.zoomSpeed;
                zoomAmount.text = "" + c.zoomAmount;
                zoomWait.text = "" + c.waitTime;
                zoomXPos.text = "" + c.zoomPosition.x;
                zoomYPos.text = "" + c.zoomPosition.y;
                break;
            case 11:
                SelectSpecificSceneType(9);
                whotoAnimateDd.value = 0;
                whotoAnimateDd.ClearOptions();
                charactersInStory = c.charactersInScene;
                List<string> whs1 = new List<string>() { };
                for (int wh = 0; wh < charactersInStory.Length; wh++)
                {
                    whs1.Add(charactersInStory[wh]);
                }
                whotoAnimateDd.AddOptions(whs1);
                animationWaitTime.text = "" + c.waitTime;
                isAnimated = c.isAnimated;
                isAnimatedToggle.isOn = isAnimated[0];
                loopAnimation = c.loopAnimation;
                animationCounters = c.animationId;
                isLoopedAnimationToggle.isOn = loopAnimation[0];
                try
                {
                    Debug.Log(c.secondaryWaitTime);
                }
                catch
                {
                    Debug.Log("Fetch error");
                }
                
                animateStartTimes = c.secondaryWaitTime;
                secondaryAnimationWaitTime.text = "" + animateStartTimes[0];
                break;
            case 13:
                SelectSpecificSceneType(12);
                textmessageInput.text = c.dialogue;
                textmessageNameInput.text = c.characterName;
                receivingToggle.isOn = c.isEnd;
                break;
            case 14:
                SelectSpecificSceneType(13);
                tooltipmessageInput.text = c.dialogue;
                break;

                
        }
    }
    IEnumerator BackToMenuIE()
    {
        //moves you back to menu
        fio.FadeOut();
        yield return new WaitForSeconds(2.0f);
        SceneManager.LoadScene("Menu");
    }
    public void BackToMenu()
    {
        //calls the back to menu with delay
        StartCoroutine(BackToMenuIE());
    }
}
