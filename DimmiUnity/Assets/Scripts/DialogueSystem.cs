﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueSystem : MonoBehaviour {

    private GameScript gs;
    private DebugScript ds;
    private CanvasGroup dcg; //dialogue canvas group
    private bool chatActive, autoSkip;
    [SerializeField] private float autoScrollSpeed, activationSpeed;
    [SerializeField] private bool skipped, nextAvailable;
    private Text dialogueText, nameText, questionText;
    private Image nextIcon;
    [SerializeField] private Color red, black;
    [SerializeField] private Vector3 inPos, inPos2, inPos3, outPos, outPos2;
    private GameObject dialoguePanel;
    private GameObject selectionButtons;
    private GameObject nextButton;
    private Text selectionOption1, selectionOption2, selectionOption3;
    public GameObject nameGo;
    public GameObject spaceGo;
    public bool selectionActive;
    public Font f1, f2;
    public GameObject nextButton2;

    // Use this for initialization
    void Start ()
    {
        //sets up all the veriables
        autoSkip = false;
        if(PlayerPrefs.GetInt("AutoSkip") == 1)
        {
            autoSkip = true;
        }

        selectionButtons = GameObject.Find("SelectionButtons");
        selectionOption1 = GameObject.Find("SelectionOptionText1").GetComponent<Text>();
        selectionOption2 = GameObject.Find("SelectionOptionText2").GetComponent<Text>();
        selectionOption3 = GameObject.Find("SelectionOptionText3").GetComponent<Text>();
        spaceGo = GameObject.Find("Space");
        nameGo = GameObject.Find("NameText");
        dialoguePanel = GameObject.Find("Dialoguebox");
        autoScrollSpeed = 0.03f;
        activationSpeed = 3.0f;
        nextIcon = GameObject.Find("NextIcon").GetComponent<Image>();
        nextIcon.gameObject.SetActive(false);
        nextButton = GameObject.Find("Dialoguebox");
        nextButton2 = GameObject.Find("NextButton");
        nameText = GameObject.Find("NameText").GetComponent<Text>();
        dialogueText = GameObject.Find("DialogueText").GetComponent<Text>();
        questionText = GameObject.Find("QuestionText").GetComponent<Text>();
        dcg = GameObject.Find("DialogueCanvas").GetComponent<CanvasGroup>();
        gs = GetComponent<GameScript>();
        ds = GetComponent<DebugScript>();
	}

	
    IEnumerator CanSelectTimer()
    {

        //the cooldown on how fast you can skip stuff.
        GameObject.Find("ButtonOne").GetComponent<Button>().interactable = false;
        GameObject.Find("ButtonTwo").GetComponent<Button>().interactable = false;
        GameObject.Find("ButtonThree").GetComponent<Button>().interactable = false;
        yield return new WaitForSeconds(1.0f);
        GameObject.Find("ButtonOne").GetComponent<Button>().interactable = true;
        GameObject.Find("ButtonTwo").GetComponent<Button>().interactable = true;
        GameObject.Find("ButtonThree").GetComponent<Button>().interactable = true;

    }
    void Update ()
    {
        LerpChatAlpha();	
	}
    void LerpChatAlpha()
    {
        //moves and fades the chat windows.
        if(chatActive)
        {
            if(selectionActive)
            {
                dcg.alpha = Mathf.Lerp(dcg.alpha, 1f, Time.deltaTime * activationSpeed);
                dialoguePanel.transform.localPosition = Vector3.Lerp(dialoguePanel.transform.localPosition, inPos2, Time.deltaTime * 7f);
                selectionButtons.transform.localPosition = Vector3.Lerp(selectionButtons.transform.localPosition, inPos3, Time.deltaTime * 7f);
            }
            if (!selectionActive)
            {
                dcg.alpha = Mathf.Lerp(dcg.alpha, 1f, Time.deltaTime * activationSpeed);
                dialoguePanel.transform.localPosition = Vector3.Lerp(dialoguePanel.transform.localPosition, inPos, Time.deltaTime * 7f);
                selectionButtons.transform.localPosition = Vector3.Lerp(selectionButtons.transform.localPosition, outPos, Time.deltaTime * 7f);
            }
        }
        if(!chatActive)
        {
            dcg.alpha = Mathf.Lerp(dcg.alpha, 0f, Time.deltaTime * activationSpeed * 4);
            dialoguePanel.transform.localPosition = Vector3.Lerp(dialoguePanel.transform.localPosition, outPos, Time.deltaTime * 10f);
            selectionButtons.transform.localPosition = Vector3.Lerp(selectionButtons.transform.localPosition, inPos3, Time.deltaTime * 7f);
        }
    }
    public void SelectionActive(bool b)
    {
        //turns on the selection
        chatActive = b;
        selectionActive = b;
        questionText.gameObject.SetActive(true);
        nameGo.SetActive(false);
        spaceGo.SetActive(false);
        nextIcon.gameObject.SetActive(false);
        dialogueText.gameObject.SetActive(false);
    }
    public void SetChatActive(bool b)
    {
        //turns on the chat
        nameGo.SetActive(true);
        spaceGo.SetActive(true);
        dialogueText.gameObject.SetActive(true);
        questionText.gameObject.SetActive(false);
        selectionButtons.SetActive(false);
        nextButton.GetComponent<Button>().enabled = true;
        chatActive = b;
        selectionActive = false;
    }
    public void StartChat(bool teller, string nam, string dialog, bool isSelect)
    {
        //start the chat
        StartCoroutine(AutoScrollText(teller, nam, dialog, isSelect));      
    }

    public void Selection(int i)
    {
        //confirm selection
        gs.Selection(i);
        nextButton2.SetActive(true);
        dialoguePanel.SetActive(true);
        SkipAndNext();
        Debug.Log("Selection Tapped");
    }
    public void SkipAndNext()
    {
        //skip the chat, and/or go to next
        if(nextAvailable)
        {
            dcg.blocksRaycasts = false;
            dcg.interactable = false;
            dialogueText.text = "";
            nameText.text = "";
            gs.NextScene();         
            nextAvailable = false;
            nextIcon.gameObject.SetActive(false);
            return;
        }
        if(!nextAvailable)
        {
            skipped = !skipped;
        }
    }
    public void EnableSelection(bool b, string s, string s1, string s2)
    {
        //shows selection window
        selectionOption1.text = s;
        selectionOption2.text = s1;
        selectionOption3.text = s2;
        selectionButtons.SetActive(true);
        nextButton2.SetActive(false);
        dialoguePanel.SetActive(false);
        nameGo.SetActive(false);
        spaceGo.SetActive(false);
        nextIcon.gameObject.SetActive(false);
        dialogueText.gameObject.SetActive(false);
        nextButton.GetComponent<Button>().enabled = false;
        selectionActive = true;

    }
    public void GetSelection()
    {
        //gets selection
        gs.EnableSelection();
        StartCoroutine(CanSelectTimer());
    }
    IEnumerator BlinkNext()
    {
        //indicator arrow that blinks showing that the text has finished and you can tap the chatbox ( not in use )
        while(nextAvailable)
        {
            nextIcon.gameObject.SetActive(!nextIcon.gameObject.activeSelf);
            yield return new WaitForSeconds(0.5f);
            if(!nextAvailable)
            {
                nextIcon.gameObject.SetActive(false);
            }
        }
    }

    IEnumerator AutoScrollText(bool b, string s, string s2, bool isSelect)
    {
        //auto scrolls the text
        //s2 = dialogue
        //s = name
        //b = storyteller/thinking
        //isSelect = selection scene or not

        string sub = "";
        nameText.text = s;
        if(isSelect)
        {
            nameText.text = "";
        }
        dialogueText.text = "";
        questionText.text = "";
        dcg.blocksRaycasts = true;
        dcg.interactable = true;
        if (b)
        {
            dialogueText.color = red;
            dialogueText.font = f2;
        }
        else
        {
            dialogueText.color = black;
            dialogueText.font = f1;
        }
        if(!autoSkip)
        {
            if (!skipped)
            {
                for (int i = 0; i < s2.Length; i++)
                {

                    yield return new WaitForSeconds(autoScrollSpeed);

                    sub = s2.Substring(0, i + 1);
                    if(isSelect)
                    {
                        questionText.text = sub;
                        skipped = true;
                    }
                    if(!isSelect)
                    {
                        dialogueText.text = sub;
                    }
                    if (skipped)
                    {
                        ds.DEB("Skipped");
                        if(!isSelect)
                        {
                            dialogueText.text = s2;
                        }
                        if (isSelect)
                        {
                            questionText.text = s2;
                        }
                        nextAvailable = true;
                        skipped = false;    
                        if (isSelect)
                        {
                            GetSelection();
                        }
                        if(!isSelect)
                        {
                         //   StartCoroutine(BlinkNext());
                        }
                        
                        yield break;
                    }
                }
                ds.DEB("Finished");
                if(isSelect)
                {
                    GetSelection();
                }
                nextAvailable = true;
                if (!isSelect)
                {
                  //  StartCoroutine(BlinkNext());
                }
            }

            if (skipped)
            {
                ds.DEB("Fully Skipped");
                if (!isSelect)
                {
                    dialogueText.text = s2;
                }
                if (isSelect)
                {
                    questionText.text = s2;
                }
                nextAvailable = true;
                if (isSelect)
                {
                    GetSelection();
                }
                skipped = false;
                if (!isSelect)
                {
                  //  StartCoroutine(BlinkNext());
                }
            }
        }
        if(autoSkip)
        {
            ds.DEB("Autoskipped");
            if (!isSelect)
            {
                dialogueText.text = s2;
            }
            if (isSelect)
            {
                questionText.text = s2;
            }
            if (isSelect)
            {
                GetSelection();
            }
            nextAvailable = true;
            if (!isSelect)
            {
               // StartCoroutine(BlinkNext());
            }
        }


    }
}
