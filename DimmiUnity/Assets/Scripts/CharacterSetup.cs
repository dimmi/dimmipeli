﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Bugsy;

public class CharacterSetup : MonoBehaviour {

    public int[] gearArray;
    public Sprite[] spriteList;
    public JsonSaveLoad jsl;
    public SpriteLibrary sl;
    public float scales;
    public Vector3 targetPosition;
    public bool moving;
    public List<Transform> t;
    public int gender;
    public float speed;
    public Color[] clothingColoring;

    //animation helps

    // Use this for initialization
    void Start ()
    {
        jsl = GameObject.Find("ScriptBlock").GetComponent<JsonSaveLoad>();
        sl = GameObject.Find("ScriptBlock").GetComponent<SpriteLibrary>();
        SetupCharacter(transform.name);
	}
    void MoveCharacter()
    {
        //moves the character and does the walk animation
        transform.position = Vector3.Lerp(transform.position, targetPosition, speed * Time.deltaTime);
        if(moving)
        {
            if ((transform.position.x - targetPosition.x) > -0.1f && (transform.position.x - targetPosition.x) < 0.1f)
            {
                moving = false;
                if(gender == 0)
                {
                    
                    GetComponent<Animation>().Play("MaleIdle");
                }
               if(gender == 1)
                {
                    
                    GetComponent<Animation>().Play("FemaleIdle");
                }
            }
            else
            {
                if(transform.position.x < targetPosition.x)
                {
                    transform.Translate(Vector3.right * speed * Time.deltaTime);
                   
                }
                if(transform.position.x > targetPosition.x)
                {
                    transform.Translate(-Vector3.right * speed * Time.deltaTime);
                }
            }
        }
        if(gender == 1)
        {
            //debug calls, not used in the game, just to test animations
            if(Input.GetKeyDown(KeyCode.Q))
            {
                GetComponent<Animation>().Stop();
                GetComponent<Animation>().Play("FemaleLaugh");
            }
            if (Input.GetKeyDown(KeyCode.W))
            {
                GetComponent<Animation>().Stop();
                GetComponent<Animation>().Play("FemaleTalk1");
            }
            if (Input.GetKeyDown(KeyCode.E))
            {
                GetComponent<Animation>().Stop();
                GetComponent<Animation>().Play("FemaleWink");
            }
            if (Input.GetKeyDown(KeyCode.R))
            {
                GetComponent<Animation>().Stop();
                GetComponent<Animation>().Play("FemalePoint");
            }
            if (Input.GetKeyDown(KeyCode.T))
            {
                GetComponent<Animation>().Stop();
                GetComponent<Animation>().Play("FemaleCry");
            }
            if (Input.GetKeyDown(KeyCode.Y))
            {
                GetComponent<Animation>().Stop();
                GetComponent<Animation>().Play("FemaleShock");
            }
            if (Input.GetKeyDown(KeyCode.A))
            {
                GetComponent<Animation>().Stop();
                GetComponent<Animation>().Play("FemaleChuckle");
            }
            if (Input.GetKeyDown(KeyCode.S))
            {
                GetComponent<Animation>().Stop();
                GetComponent<Animation>().Play("FemaleAgree");
            }
            if (Input.GetKeyDown(KeyCode.D))
            {
                GetComponent<Animation>().Stop();
                GetComponent<Animation>().Play("FemaleDisagree");
            }
            if (Input.GetKeyDown(KeyCode.F))
            {
                GetComponent<Animation>().Stop();
                GetComponent<Animation>().Play("FemaleAngry");
            }
            if (Input.GetKeyDown(KeyCode.G))
            {
                GetComponent<Animation>().Stop();
                GetComponent<Animation>().Play("FemaleWalk");
            }

        }
    }
    public void Eyenimation(int eyeInt)
    {
        //moves the other eye according to animations
        if (gender == 0)
        {
            switch (eyeInt)
            {
                case 0:
                    t.Find(x => x.name == "Eye2").GetComponent<SpriteRenderer>().sprite = sl.manrighteye1[7 * gearArray[3] + gearArray[1]];
                    break;
                case 1:
                    t.Find(x => x.name == "Eye2").GetComponent<SpriteRenderer>().sprite = sl.manrighteye2[7 * gearArray[3] + gearArray[1]];
                    break;
                case 2:
                    t.Find(x => x.name == "Eye2").GetComponent<SpriteRenderer>().sprite = sl.manrighteye3[7 * gearArray[3] + gearArray[1]];
                    break;
            }
            
        }
        if (gender == 1)
        {
            switch (eyeInt)
            {
                case 0:
                    t.Find(x => x.name == "Eye2").GetComponent<SpriteRenderer>().sprite = sl.womanrighteye1[7 * gearArray[3] + gearArray[1]];
                    Debug.Log("Open");
                    break;
                case 1:
                    t.Find(x => x.name == "Eye2").GetComponent<SpriteRenderer>().sprite = sl.womanrighteye2[7 * gearArray[3] + gearArray[1]];
                    Debug.Log("Mid");
                    break;
                case 2:
                    t.Find(x => x.name == "Eye2").GetComponent<SpriteRenderer>().sprite = sl.womanrighteye3[7 * gearArray[3] + gearArray[1]];
                    Debug.Log("Closed");
                    break;
            }

        }
    }
    public void Eyenimation2(int eyeInt)
    {
        //moves the other eye according to animations
        if (gender == 0)
        {

            switch (eyeInt)
            {
                case 0:
                    t.Find(x => x.name == "Eye1").GetComponent<SpriteRenderer>().sprite = sl.manlefteye1[7 * gearArray[3] + gearArray[1]];
                    break;
                case 1:
                    t.Find(x => x.name == "Eye1").GetComponent<SpriteRenderer>().sprite = sl.manlefteye2[7 * gearArray[3] + gearArray[1]];
                    break;
                case 2:
                    t.Find(x => x.name == "Eye1").GetComponent<SpriteRenderer>().sprite = sl.manlefteye3[7 * gearArray[3] + gearArray[1]];
                    break;
            }
        

        }
        if (gender == 1)
        {
            switch (eyeInt)
            {
                case 0:
                    t.Find(x => x.name == "Eye1").GetComponent<SpriteRenderer>().sprite = sl.womanlefteye1[7 * gearArray[3] + gearArray[1]];
                    Debug.Log("Open");
                    break;
                case 1:
                    t.Find(x => x.name == "Eye1").GetComponent<SpriteRenderer>().sprite = sl.womanlefteye2[7 * gearArray[3] + gearArray[1]];
                    Debug.Log("Mid");
                    break;
                case 2:
                    t.Find(x => x.name == "Eye1").GetComponent<SpriteRenderer>().sprite = sl.womanlefteye3[7 * gearArray[3] + gearArray[1]];
                    Debug.Log("Closed");
                    break;
            }

        }
    }

    public void Mouthimation(int mouthInt)
    {
        //moves the mouth according to animations
        int x = 6 * gearArray[2];
        
        if(gender == 0)
        {
            t.Find(f => f.name == "Mouth").GetComponent<SpriteRenderer>().sprite = sl.manmouth[ mouthInt];
        }
        if(gender == 1)
        {
            t.Find(f => f.name == "Mouth").GetComponent<SpriteRenderer>().sprite = sl.womanmouth[gearArray[2] * 6 + mouthInt];
        }
    }
    public void SetPosition(Vector3 v, int i)
    {
        //moves character to wanted position
        if(i == 0)
        {
            targetPosition = v;
            if((targetPosition.x - transform.position.x) < -0.1f || (targetPosition.x - transform.position.x) > 0.1f)
            {
                moving = true;
                if(gender == 0)
                {
                    GetComponent<Animation>().Stop();
                    GetComponent<Animation>().Play("MaleWalk");
                }
                if (gender == 1)
                {
                    GetComponent<Animation>().Stop();
                    GetComponent<Animation>().Play("FemaleWalk");
                }
                
                //play walking animation;
            }
        }
        if(i == 1)
        {
            targetPosition = transform.position;
        }
        
        
    }
    void Update()
    {

        MoveCharacter();
        //checks if an animation is playing and if one is not, starts the idle animation
        if(GetComponent<Animation>().isPlaying == false)
        {
            if(gender == 0)
            {
                GetComponent<Animation>().Play("MaleIdle");
            }
            if (gender == 1)
            {
                GetComponent<Animation>().Play("FemaleIdle");
            }
        }
    }
    public void ChangeClothes(int clothSpot, int clothId)
    {
        //changes the character clothes from change clothe scenes ( not made since ran out of time, and was not necessary in stories )
        switch(clothSpot)
        {
            case 0:
                if(gender == 0)
                {
                    t[clothSpot].GetComponent<SpriteRenderer>().sprite = null;
                }
                if (gender == 1)
                {
                    t[clothSpot].GetComponent<SpriteRenderer>().sprite = null;
                }
                break;
            case 1:
                if (gender == 0)
                {
                    t[clothSpot].GetComponent<SpriteRenderer>().sprite = null;
                }
                if (gender == 1)
                {
                    t[clothSpot].GetComponent<SpriteRenderer>().sprite = null;
                }
                break;
            case 2:
                if (gender == 0)
                {
                    t[clothSpot].GetComponent<SpriteRenderer>().sprite = null;
                }
                if (gender == 1)
                {
                    t[clothSpot].GetComponent<SpriteRenderer>().sprite = null;
                }
                break;
            case 3:
                if (gender == 0)
                {
                    t[clothSpot].GetComponent<SpriteRenderer>().sprite = null;
                }
                if (gender == 1)
                {
                    t[clothSpot].GetComponent<SpriteRenderer>().sprite = null;
                }
                break;
            case 4:
                if (gender == 0)
                {
                    t[clothSpot].GetComponent<SpriteRenderer>().sprite = null;
                }
                if (gender == 1)
                {
                    t[clothSpot].GetComponent<SpriteRenderer>().sprite = null;
                }
                break;
            case 5:
                if (gender == 0)
                {
                    t[clothSpot].GetComponent<SpriteRenderer>().sprite = null;
                }
                if (gender == 1)
                {
                    t[clothSpot].GetComponent<SpriteRenderer>().sprite = null;
                }
                break;
            case 6:
                if (gender == 0)
                {
                    t[clothSpot].GetComponent<SpriteRenderer>().sprite = null;
                }
                if (gender == 1)
                {
                    t[clothSpot].GetComponent<SpriteRenderer>().sprite = null;
                }
                break;
            case 7:
                if (gender == 0)
                {
                    t[clothSpot].GetComponent<SpriteRenderer>().sprite = null;
                }
                if (gender == 1)
                {
                    t[clothSpot].GetComponent<SpriteRenderer>().sprite = null;
                }
                break;
            case 8:
                if (gender == 0)
                {
                    t[clothSpot].GetComponent<SpriteRenderer>().sprite = null;
                }
                if (gender == 1)
                {
                    t[clothSpot].GetComponent<SpriteRenderer>().sprite = null;
                }
                break;
            case 9:
                if (gender == 0)
                {
                    t[clothSpot].GetComponent<SpriteRenderer>().sprite = null;
                }
                if (gender == 1)
                {
                    t[clothSpot].GetComponent<SpriteRenderer>().sprite = null;
                }
                break;
            case 10:
                if (gender == 0)
                {
                    t[clothSpot].GetComponent<SpriteRenderer>().sprite = null;
                }
                if (gender == 1)
                {
                    t[clothSpot].GetComponent<SpriteRenderer>().sprite = null;
                }
                break;
            case 11:
                if (gender == 0)
                {
                    t[clothSpot].GetComponent<SpriteRenderer>().sprite = null;
                }
                if (gender == 1)
                {
                    t[clothSpot].GetComponent<SpriteRenderer>().sprite = null;
                }
                break;
            case 12:
                if (gender == 0)
                {
                    t[clothSpot].GetComponent<SpriteRenderer>().sprite = null;
                }
                if (gender == 1)
                {
                    t[clothSpot].GetComponent<SpriteRenderer>().sprite = null;
                }
                break;
            case 13:
                if (gender == 0)
                {
                    t[clothSpot].GetComponent<SpriteRenderer>().sprite = null;
                }
                if (gender == 1)
                {
                    t[clothSpot].GetComponent<SpriteRenderer>().sprite = null;
                }
                break;
            case 14:
                if (gender == 0)
                {
                    t[clothSpot].GetComponent<SpriteRenderer>().sprite = null;
                }
                if (gender == 1)
                {
                    t[clothSpot].GetComponent<SpriteRenderer>().sprite = null;
                }
                break;
        }
       
    }
    public void ScaleCharacter(Vector3 v)
    {
        //scales the character
        transform.localScale = v;
    }
    public void TurnCharacter()
    {
        //turns the character
        if(transform.localScale.x < 0)
        {
            transform.localScale = new Vector3(scales, scales, 1f);
            return;
        }
        if(transform.localScale.x > 0)
        {
            transform.localScale = new Vector3(-scales, scales, 1f);
        }

    }
    public void SetScales(float f)
    {
        //scales the character
        scales = f;
    }

    public void SetRotation(bool b)
    {
        //false = looking left
        if(!b)
        {
            Debug.Log("CS: " + scales);
            transform.localScale = new Vector3(scales, scales, 1f);
        }
        if (b)
        {
            Debug.Log("CS: " + -scales);
            transform.localScale = new Vector3(-scales, scales, 1f);
        }
    }

    public void SetupCharacter(string s)
    {   
       //sets up the character basec on values set in character editor
        Character c = new Character();
 
        if(PlayerPrefs.GetInt("Custom") == 1)
        {
            c = jsl.GetCharacter(s);
        }
        if (PlayerPrefs.GetInt("Custom") == 0)
        {
            c = jsl.GetResourceCharacter(s);
        }
        gearArray = c.gear;
        foreach(Transform child in transform.GetComponentsInChildren<Transform>())
        {
            if(child == transform)
            {
                Debug.Log("err - parent object, ignored");
            }
            if(child != transform)      
            {
                t.Add(child);
                Debug.Log(t.Count + " " + child.name);
            }
        }
        gender = c.gender;
        string tmp = "";

        switch(c.gear[3])
        {
            case 0:
                tmp = "L";
                break;
            case 1:
                tmp = "M";
                break;
            case 2:
                tmp = "D";
                break;
        }

        List<Sprite> tmpspr = new List<Sprite>() { };
        List<Sprite> tmpspr2 = new List<Sprite>() { };
        if (gender == 0)
        {
            tmpspr = Debugsy.ArrayToList(sl.manAllSkins);
            tmpspr2 = Debugsy.ArrayToList(sl.manAllClothes);
        }
        if(gender == 1)
        {
            tmpspr = Debugsy.ArrayToList(sl.womanAllSkins);
            tmpspr2 = Debugsy.ArrayToList(sl.womanAllClothes);
        }

        if (c.gender == 0)
        {
            //set male gear
            
            t.Find(x => x.name == "MainHair").GetComponent<SpriteRenderer>().sprite =  c.gear[9] == 1 ? sl.manhatHair[c.gear[0]] : sl.manHair[c.gear[0]];//hair
            t.Find(x => x.name == "Brow1").GetComponent<SpriteRenderer>().sprite = sl.manbrow1[c.gear[0]];
            t.Find(x => x.name == "Brow2").GetComponent<SpriteRenderer>().sprite = sl.manbrow2[c.gear[0]];
            t.Find(x => x.name == "Eye1").GetComponent<SpriteRenderer>().sprite = sl.manlefteye1[7 * c.gear[3] + c.gear[1]];
            t.Find(x => x.name == "Eye2").GetComponent<SpriteRenderer>().sprite = sl.manrighteye1[7 * c.gear[3] + c.gear[1]];
            t.Find(x => x.name == "Mouth").GetComponent<SpriteRenderer>().sprite = sl.manmouth[c.gear[2] * 6 + 4];
            t.Find(x => x.name == "Nose").GetComponent<SpriteRenderer>().sprite = sl.manNose[c.gear[3]];
            t.Find(x => x.name == "Head").GetComponent<SpriteRenderer>().sprite = tmpspr.Find(x => x.name == tmp + "Head");
            t.Find(x => x.name == "Ear").GetComponent<SpriteRenderer>().sprite = tmpspr.Find(x => x.name == tmp + "Ear");
            t.Find(x => x.name == "LeftHand").GetComponent<SpriteRenderer>().sprite = tmpspr.Find(x => x.name == tmp + "Hand");
            t.Find(x => x.name == "RightHand").GetComponent<SpriteRenderer>().sprite = tmpspr.Find(x => x.name == tmp + "Hand");
            t.Find(x => x.name == "Upperbody").GetComponent<SpriteRenderer>().sprite = tmpspr.Find(x => x.name == tmp + "Upperbody");
            t.Find(x => x.name == "LowerBody").GetComponent<SpriteRenderer>().sprite = tmpspr.Find(x => x.name == tmp + "LowerBody");
            t.Find(x => x.name == "LeftUpperArm").GetComponent<SpriteRenderer>().sprite = tmpspr.Find(x => x.name == tmp + "LeftUpperArm");
            t.Find(x => x.name == "LeftLowerArm").GetComponent<SpriteRenderer>().sprite = tmpspr.Find(x => x.name == tmp + "LeftLowerArm");
            t.Find(x => x.name == "RightUpperArm").GetComponent<SpriteRenderer>().sprite = tmpspr.Find(x => x.name == tmp + "RightUpperArm");
            t.Find(x => x.name == "RightLowerArm").GetComponent<SpriteRenderer>().sprite = tmpspr.Find(x => x.name == tmp + "RightLowerArm");
            t.Find(x => x.name == "RightThigh").GetComponent<SpriteRenderer>().sprite = tmpspr.Find(x => x.name == tmp + "RightThigh");
            t.Find(x => x.name == "LeftThigh").GetComponent<SpriteRenderer>().sprite = tmpspr.Find(x => x.name == tmp + "LeftThigh");
            t.Find(x => x.name == "LeftLeg").GetComponent<SpriteRenderer>().sprite = tmpspr.Find(x => x.name == tmp + "LeftLeg");
            t.Find(x => x.name == "RightLeg").GetComponent<SpriteRenderer>().sprite = tmpspr.Find(x => x.name == tmp + "RightLeg");

            t.Find(x => x.name == "ShirtTop").GetComponent<SpriteRenderer>().sprite = tmpspr2.Find(x => x.name == "ShirtTop" + c.gear[4]);
            t.Find(x => x.name == "ShirtBottom").GetComponent<SpriteRenderer>().sprite = tmpspr2.Find(x => x.name == "ShirtBottom" + c.gear[4]);
            t.Find(x => x.name == "LeftUpperSleeve").GetComponent<SpriteRenderer>().sprite = tmpspr2.Find(x => x.name == "LeftUpperSleeve" + c.gear[4]);
            t.Find(x => x.name == "RightUpperSleeve").GetComponent<SpriteRenderer>().sprite = tmpspr2.Find(x => x.name == "RightUpperSleeve" + c.gear[4]);

            try
            {
                t.Find(x => x.name == "RightLowerSleeve").GetComponent<SpriteRenderer>().sprite = tmpspr2.Find(x => x.name == "RightLowerSleeve" + c.gear[4]);
                t.Find(x => x.name == "LeftLowerSleeve").GetComponent<SpriteRenderer>().sprite = tmpspr2.Find(x => x.name == "LeftLowerSleeve" + c.gear[4]);
            }
            catch
            {
                print("no lower sleeves");
            }

            t.Find(x => x.name == "PantWaist").GetComponent<SpriteRenderer>().sprite = tmpspr2.Find(x => x.name == "PantWaist");
            t.Find(x => x.name == "LeftUpperPant").GetComponent<SpriteRenderer>().sprite = tmpspr2.Find(x => x.name == "LeftUpperPant");
            t.Find(x => x.name == "LeftLowerPant").GetComponent<SpriteRenderer>().sprite = tmpspr2.Find(x => x.name == "LeftLowerPant");
            t.Find(x => x.name == "RightUpperPant").GetComponent<SpriteRenderer>().sprite = tmpspr2.Find(x => x.name == "RightUpperPant");
            t.Find(x => x.name == "RightLowerPant").GetComponent<SpriteRenderer>().sprite = tmpspr2.Find(x => x.name == "RightLowerPant");

            t.Find(x => x.name == "PantWaist").GetComponent<SpriteRenderer>().color = clothingColoring[c.gear[7]];
            t.Find(x => x.name == "LeftUpperPant").GetComponent<SpriteRenderer>().color = clothingColoring[c.gear[7]];
            t.Find(x => x.name == "LeftLowerPant").GetComponent<SpriteRenderer>().color = clothingColoring[c.gear[7]];
            t.Find(x => x.name == "RightUpperPant").GetComponent<SpriteRenderer>().color = clothingColoring[c.gear[7]];
            t.Find(x => x.name == "RightLowerPant").GetComponent<SpriteRenderer>().color = clothingColoring[c.gear[7]];

            t.Find(x => x.name == "ShirtTop").GetComponent<SpriteRenderer>().color = clothingColoring[c.gear[5]];
            t.Find(x => x.name == "ShirtBottom").GetComponent<SpriteRenderer>().color = clothingColoring[c.gear[5]];
            t.Find(x => x.name == "LeftUpperSleeve").GetComponent<SpriteRenderer>().color = clothingColoring[c.gear[5]];
            t.Find(x => x.name == "RightUpperSleeve").GetComponent<SpriteRenderer>().color = clothingColoring[c.gear[5]];

            try
            {
                t.Find(x => x.name == "RightLowerSleeve").GetComponent<SpriteRenderer>().color = clothingColoring[c.gear[5]];
                t.Find(x => x.name == "LeftLowerSleeve").GetComponent<SpriteRenderer>().color = clothingColoring[c.gear[5]];
            }
            catch
            {
                print("no lower sleeves");
            }

            t.Find(x => x.name == "LeftFoot").GetComponent<SpriteRenderer>().color = clothingColoring[c.gear[8]];
            t.Find(x => x.name == "RightFoot").GetComponent<SpriteRenderer>().color = clothingColoring[c.gear[8]];

            t.Find(x => x.name == "Hat").GetComponent<SpriteRenderer>().sprite = c.gear[9] == 1 ? tmpspr2.Find(x => x.name == "Cap") : null;
            t.Find(x => x.name == "Logo").GetComponent<SpriteRenderer>().sprite = c.gear[10] == 1 ? sl.logo[0] : null;
            t.Find(x => x.name == "FaceDecal").GetComponent<SpriteRenderer>().sprite = c.gear[11] == 1 ? sl.old[0] : null;
            t.Find(x => x.name == "Beard").GetComponent<SpriteRenderer>().sprite = c.gear[11] == 1 ? sl.old[1] : null;
        }
        if (c.gender == 1)
        {
            //set female gear
            Debug.Log(c.gear[3] + "&&" + gearArray[3]);
            t.Find(x => x.name == "MainHair").GetComponent<SpriteRenderer>().sprite = sl.womanHair1[c.gear[0]]; //hair
            t.Find(x => x.name == "FrontHair").GetComponent<SpriteRenderer>().sprite = sl.womanHair2[c.gear[0]]; //fronthair
            t.Find(x => x.name == "BackHair").GetComponent<SpriteRenderer>().sprite = sl.womanHair3[c.gear[0]]; //backhair
            t.Find(x => x.name == "Eye1").GetComponent<SpriteRenderer>().sprite = sl.womanlefteye1[7 * c.gear[3] + c.gear[1]];
            t.Find(x => x.name == "Eye2").GetComponent<SpriteRenderer>().sprite = sl.womanrighteye1[7 * c.gear[3] + c.gear[1]];
            t.Find(x => x.name == "Brow1").GetComponent<SpriteRenderer>().sprite = sl.womanbrowleft[c.gear[0]]; //fronthair
            t.Find(x => x.name == "Brow2").GetComponent<SpriteRenderer>().sprite = sl.womanbrowright[c.gear[0]]; //backhair
            t.Find(x => x.name == "Mouth").GetComponent<SpriteRenderer>().sprite = sl.womanmouth[c.gear[2]*6 + 4];
            t.Find(x => x.name == "Head").GetComponent<SpriteRenderer>().sprite = tmpspr.Find(x => x.name == tmp + "Head");
            t.Find(x => x.name == "Ear").GetComponent<SpriteRenderer>().sprite = tmpspr.Find(x => x.name == tmp + "Ear");
            t.Find(x => x.name == "LeftHand").GetComponent<SpriteRenderer>().sprite = tmpspr.Find(x => x.name == tmp + "Hand");
            t.Find(x => x.name == "RightHand").GetComponent<SpriteRenderer>().sprite = tmpspr.Find(x => x.name == tmp + "Hand");
            t.Find(x => x.name == "Upperbody").GetComponent<SpriteRenderer>().sprite = tmpspr.Find(x => x.name == tmp + "Upperbody");
            t.Find(x => x.name == "LowerBody").GetComponent<SpriteRenderer>().sprite = tmpspr.Find(x => x.name == tmp + "LowerBody");
            t.Find(x => x.name == "LeftUpperArm").GetComponent<SpriteRenderer>().sprite = tmpspr.Find(x => x.name == tmp + "LeftUpperArm");
            t.Find(x => x.name == "LeftLowerArm").GetComponent<SpriteRenderer>().sprite = tmpspr.Find(x => x.name == tmp + "LeftLowerArm");
            t.Find(x => x.name == "RightUpperArm").GetComponent<SpriteRenderer>().sprite = tmpspr.Find(x => x.name == tmp + "RightUpperArm");
            t.Find(x => x.name == "RightLowerArm").GetComponent<SpriteRenderer>().sprite = tmpspr.Find(x => x.name == tmp + "RightLowerArm");
            t.Find(x => x.name == "RightThigh").GetComponent<SpriteRenderer>().sprite = tmpspr.Find(x => x.name == tmp + "RightThigh");
            t.Find(x => x.name == "LeftThigh").GetComponent<SpriteRenderer>().sprite = tmpspr.Find(x => x.name == tmp + "LeftThigh");
            t.Find(x => x.name == "LeftLeg").GetComponent<SpriteRenderer>().sprite = tmpspr.Find(x => x.name == tmp + "LeftLeg");
            t.Find(x => x.name == "RightLeg").GetComponent<SpriteRenderer>().sprite = tmpspr.Find(x => x.name == tmp + "RightLeg");
            t.Find(x => x.name == "Nose").GetComponent<SpriteRenderer>().sprite = sl.womanNose[c.gear[3]];

            t.Find(x => x.name == "ShirtTop").GetComponent<SpriteRenderer>().sprite = tmpspr2.Find(x => x.name == "ShirtTop" + c.gear[4]);
            t.Find(x => x.name == "ShirtBottom").GetComponent<SpriteRenderer>().sprite = tmpspr2.Find(x => x.name == "ShirtBottom" + c.gear[4]);
            t.Find(x => x.name == "LeftUpperSleeve").GetComponent<SpriteRenderer>().sprite = tmpspr2.Find(x => x.name == "LeftUpperSleeve" + c.gear[4]);
            t.Find(x => x.name == "RightUpperSleeve").GetComponent<SpriteRenderer>().sprite = tmpspr2.Find(x => x.name == "RightUpperSleeve" + c.gear[4]);

            try
            {
                t.Find(x => x.name == "RightLowerSleeve").GetComponent<SpriteRenderer>().sprite = tmpspr2.Find(x => x.name == "RightLowerSleeve" + c.gear[4]);
                t.Find(x => x.name == "LeftLowerSleeve").GetComponent<SpriteRenderer>().sprite = tmpspr2.Find(x => x.name == "LeftLowerSleeve" + c.gear[4]);
            }
            catch
            {
                print("no lower sleeves");
            }

            t.Find(x => x.name == "PantWaist").GetComponent<SpriteRenderer>().sprite = tmpspr2.Find(x => x.name == "PantWaist");
            t.Find(x => x.name == "LeftUpperPant").GetComponent<SpriteRenderer>().sprite = tmpspr2.Find(x => x.name == "LeftUpperPant");
            t.Find(x => x.name == "LeftLowerPant").GetComponent<SpriteRenderer>().sprite = tmpspr2.Find(x => x.name == "LeftLowerPant");
            t.Find(x => x.name == "RightUpperPant").GetComponent<SpriteRenderer>().sprite = tmpspr2.Find(x => x.name == "RightUpperPant");
            t.Find(x => x.name == "RightLowerPant").GetComponent<SpriteRenderer>().sprite = tmpspr2.Find(x => x.name == "RightLowerPant");

            t.Find(x => x.name == "ShirtTop").GetComponent<SpriteRenderer>().color = clothingColoring[c.gear[5]];
            t.Find(x => x.name == "ShirtBottom").GetComponent<SpriteRenderer>().color = clothingColoring[c.gear[5]];
            t.Find(x => x.name == "LeftUpperSleeve").GetComponent<SpriteRenderer>().color = clothingColoring[c.gear[5]];
            t.Find(x => x.name == "RightUpperSleeve").GetComponent<SpriteRenderer>().color = clothingColoring[c.gear[5]];

            try
            {
                t.Find(x => x.name == "RightLowerSleeve").GetComponent<SpriteRenderer>().color = clothingColoring[c.gear[5]];
                t.Find(x => x.name == "LeftLowerSleeve").GetComponent<SpriteRenderer>().color = clothingColoring[c.gear[5]];
            }
            catch
            {
                print("no lower sleeves");
            }

            t.Find(x => x.name == "PantWaist").GetComponent<SpriteRenderer>().color = clothingColoring[c.gear[7]];
            t.Find(x => x.name == "LeftUpperPant").GetComponent<SpriteRenderer>().color = clothingColoring[c.gear[7]];
            t.Find(x => x.name == "LeftLowerPant").GetComponent<SpriteRenderer>().color = clothingColoring[c.gear[7]];
            t.Find(x => x.name == "RightUpperPant").GetComponent<SpriteRenderer>().color = clothingColoring[c.gear[7]];
            t.Find(x => x.name == "RightLowerPant").GetComponent<SpriteRenderer>().color = clothingColoring[c.gear[7]];

            t.Find(x => x.name == "LeftFoot").GetComponent<SpriteRenderer>().color = clothingColoring[c.gear[8]];
            t.Find(x => x.name == "RightFoot").GetComponent<SpriteRenderer>().color = clothingColoring[c.gear[8]];

            t.Find(x => x.name == "Hat").GetComponent<SpriteRenderer>().sprite = c.gear[9] == 1 ? tmpspr2.Find(x => x.name == "Cap") : null;
            t.Find(x => x.name == "Logo").GetComponent<SpriteRenderer>().sprite = c.gear[10] == 1 ? sl.logo[0] : null;
            t.Find(x => x.name == "FaceDecal").GetComponent<SpriteRenderer>().sprite = c.gear[11] == 1 ? sl.old[0] : null;

        }
    }
}
