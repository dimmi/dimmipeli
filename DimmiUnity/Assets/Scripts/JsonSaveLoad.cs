﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using Bugsy;

public class JsonSaveLoad : MonoBehaviour
{

    //Character Edit
    public bool hidden;
    public string cname;
    public int gender;
    public int[] gearArray;
    public Character[] cArray;
    public List<string> characterList;
    public string[] charray;
    //public DebugScript bugsy;
    public string[] sarray;
    public string path;
    public string storypath;
    //Scene Edit
    public string sceneName;
    public Text dt;

    void Awake()
    {
   
        #if UNITY_ANDROID
        path = Application.persistentDataPath;
        
        Debug.Log("UNITY_ANDROID");
        #endif
        #if UNITY_EDITOR
        path = Application.dataPath;
        Debug.Log("UNITY_EDITOR");
        #endif
        #if UNITY_STANDALONE
        path = Application.dataPath;
        Debug.Log("UNITY_STANDALONE");
        #endif
        #if UNITY_IOS
        path = Application.persistentDataPath;
        Debug.Log("UNITY_IOS");
        #endif
        Debug.Log(path);
        dt = GameObject.Find("DebugText").GetComponent<Text>();
        //dt.text = "1";
       // dt.text = "2";
        //bugsy = GetComponent<DebugScript>();
        characterList = new List<string>();
        dt.text = path;
    }
    public CharacterLista LoadCharacterLista()
    {
        //loads the class CharacterLista
        CharacterLista c = new CharacterLista();
        dt.text = path;
        string jsonString = File.ReadAllText(path + "/JSON/GameCharacters/CharacterList.json");
        dt.text = "loaded";
        JsonUtility.FromJsonOverwrite(jsonString, c);
        return c;
    }
    public List<string> LoadCharacterArray()
    {
        //Loads the character list in a list
        dt.text = "load character array start";
        CharacterLista c = new CharacterLista();
        string jsonString = File.ReadAllText(path + "/JSON/GameCharacters/CharacterList.json");

        dt.text = "loaded";
        JsonUtility.FromJsonOverwrite(jsonString, c);
        dt.text = "overwritten";
        return c.characters;
    }
    public List<string> GetCharacterNames()
    {
        //gets character name list as a list
        CharacterLista c = new CharacterLista();
        List<string> s = new List<string>();
        string jsonString = File.ReadAllText(path + "/JSON/GameCharacters/CharacterList.json");
        JsonUtility.FromJsonOverwrite(jsonString, c);
        s = c.characters;
        return s;
    }
    public string[] LoadCharray()
    {
        //loads character name list as array
        dt.text = "load character array start";
        CharacterArray c = new CharacterArray();
        string jsonString = File.ReadAllText(path + "/JSON/GameCharacters/CharacterList.json");
                                                  
        dt.text = "loaded";
        JsonUtility.FromJsonOverwrite(jsonString, c);
        dt.text = "overwritten";
        return c.characters;
    }

    public void SaveCharray(string name)
    {
        //saves the character array class to json
        CharacterArray c = new CharacterArray();
        string[] s = LoadCharray();
        for(int x = 0; x < s.Length; x++)
        {
            if(s[x] == name)
            {
                return;
            }
        }
        string[] s2 = new string[s.Length + 1];
        for(int i = 0; i < s.Length; i++)
        {
            s2[i] = s[i];
        }
        s2[s.Length] = name;
        c.characters = s2;
        string jsonString = JsonUtility.ToJson(c);
        File.WriteAllText(path + "/JSON/GameCharacters/CharacterList.json", jsonString);
    }
    public void LoadCharacter(string s)
    {
        //loads the character class
        Character c = new Character();

        string jsonString = File.ReadAllText(path + "/JSON/GameCharacters/" + s + ".json");
        JsonUtility.FromJsonOverwrite(jsonString, c);

        name = c.name;
        gender = c.gender;
        gearArray = c.gear;

        GameObject.Find("ScriptBlock").GetComponent<CharacterEditor>().SetArrays(name, gender, gearArray);
        //bugsy.DEB("Character " + name + " Loaded.");
    }
    public Character GetResourceCharacter(string s)
    {
        //loads the character from resources
        Character c = new Character();
        string p = "JSON/GameCharacters/" + s + ".json";
        string p2 = p.Replace(".json", "");
        Debug.Log(p2);
        TextAsset t = Resources.Load<TextAsset>(p2);
        string jsonString = t.text;
        JsonUtility.FromJsonOverwrite(jsonString, c);
        return c;
    }
    public CharacterLista GetResourceCharacterList()
    {
        //loads the characterlist from resources
        CharacterLista c = new CharacterLista();
        string p = "JSON/GameCharacters/CharacterList.json";
        string p2 = p.Replace(".json", "");
        Debug.Log(p2);
        TextAsset t = Resources.Load<TextAsset>(p2);
        string jsonString = t.text;
        JsonUtility.FromJsonOverwrite(jsonString, c);
        return c;
    }
    public Character GetCharacter(string s)
    {
        //gets the character class
        Character c = new Character();

        string jsonString = File.ReadAllText(path + "/JSON/GameCharacters/" + s + ".json");
        JsonUtility.FromJsonOverwrite(jsonString, c);
        //bugsy.DEB("Character " + " Got.");

        return c;
    }
    public void SaveCharacterToJson(Character c)
    {
        //saves the character class to json
        string cString = JsonUtility.ToJson(c);
        File.WriteAllText(path + "/JSON/GameCharacters/" + c.name + ".json", cString);
        Debug.Log("saved character");
        //bugsy.DEB("Character " + s + " Saved");
        SaveCharray(c.name);
    }
    public void SavePlaceholderCharacter()
    {
        //saves placeholder characters
        Character c = new Character();

        c.name = "Pete";
        c.gender = 1;
        c.gear = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

        string cString = JsonUtility.ToJson(c);
        File.WriteAllText(path + "/JSON/GameCharacters/" + "Pete" + ".json", cString);
        //bugsy.DEB("Character " + s + " Saved");
    }
    public void SaveNewCharray()
    {
        //saves a new characterlist debug iirc
        CharacterArray c = new CharacterArray();
        string[] s = new string[1];
        s[0] = "Pete";
        c.characters = s;
        string jsonString = JsonUtility.ToJson(c);
        File.WriteAllText(path + "/JSON/GameCharacters/CharacterList.json", jsonString);
    }
    public bool DirectoryExists(string s)
    {
        //checks if directory exists and returns bool value accordingly
        bool b = false;

        if(Directory.Exists(path + "JSON/Saves/" + s))
        {
            b = true;
        }
        else
        {
            b = false;
        }
        return b;
    }
    public void FirstTimeSetup()
    {
        //first time setup
        dt.text = "creating classes";
        CharacterArray ca = new CharacterArray();
        StoryList sa = new StoryList();
        SceneArray sa1 = new SceneArray();
        Character ch = new Character();
        ch.name = "Pete";
        dt.text = "creating arrays";
        sa.story = new string[1];
        sa.story[0] = "TestStory";
        sa.hidden = new bool[1];
        sa.hidden[0] = false;
        sa.password = new string[1];
        sa.password[0] = "asd";
        ca.characters = new string[1];
        ca.characters[0] = "Pete";
        dt.text = "created arrays";
        string s = JsonUtility.ToJson(ca);
        string s2 = JsonUtility.ToJson(sa);
        string s3 = JsonUtility.ToJson(ch);
        string s4 = JsonUtility.ToJson(sa1);
        dt.text = "Converted to json, starting write.";
        
        Directory.CreateDirectory(path + "/JSON");
        Directory.CreateDirectory(path + "/JSON/GameCharacters");
        Directory.CreateDirectory(path + "/JSON/Stories");
        Directory.CreateDirectory(path + "/JSON/Stories/TestStory");
        dt.text = "Directories created";


        PlayerPrefs.SetInt("Sounds", 1);

    }
    public void SetHidden(bool b)
    {
        //sets hidden value
        hidden = b;
    }
    public void CheckHidden(string s)
    {
        //checks if is hidden
        StoryList sl = new StoryList();
        string jsonString = File.ReadAllText(path + "/JSON/Stories/HiddenStoryList.json");
        JsonUtility.FromJsonOverwrite(jsonString, sl);
        for(int i = 0; i < sl.story.Length; i++)
        {
            if(s == sl.story[i])
            {
                hidden = true;
                return;
            }
        }
        hidden = false;
    }
    public string CheckPassword(string s)
    {
        //checks if there is a hidden   level that matches the password
        string lvl = "";
        StoryList sl = LoadResourcesStoryListClass(true);
        for(int i = 0; i < sl.password.Length; i++)
        {
            if(s == sl.password[i])
            {
                lvl = sl.story[i];Debug.Log(lvl);
            }
        }
        return lvl;
    }
    public void CreateDirectory(string s)
    {
        //creates a new directory
        Directory.CreateDirectory(path + "/JSON/Saves/" + s);
    }
    public void SaveStoryToJson(string name, bool hide, string password)
    {
        //saves a new story to json
        StoryList sl = new StoryList();
        SceneArray sa = new SceneArray();

        sl = LoadStoryListClass(hidden);

        string[] sca = sl.story;
        bool[] bo = sl.hidden;
        string[] pw = sl.password;
        string[] newsca = new string[sca.Length + 1];
        bool[] newbo = new bool[bo.Length + 1];
        string[] newpw = new string[pw.Length + 1];
        for(int i = 0; i < newsca.Length-1; i++)
        {
            newsca[i] = sca[i];
            newbo[i] = bo[i];
            newpw[i] = pw[i];
        }
        newsca[sca.Length] = name;
        newbo[bo.Length] = hide;
        newpw[pw.Length] = password;
        sl.story = newsca;
        sl.hidden = newbo;
        sl.password = newpw;
        string json1 = JsonUtility.ToJson(sl);
        string json2 = JsonUtility.ToJson(sa);
        Directory.CreateDirectory(path + "/JSON/Stories/" + name);
        File.WriteAllText(path + "/JSON/Stories/" + name + "/SceneList.json", json2);
        if (hide)
        {
            File.WriteAllText(path + "/JSON/Stories/HiddenStoryList.json", json1);
        }
        if (!hide)
        {
            File.WriteAllText(path + "/JSON/Stories/StoryList.json", json1);
        }
    }
   
    public string[] LoadStoryList()
    {
        //loads a story list array
        StoryList sli = new StoryList();

        string jsonString = File.ReadAllText(path + "/JSON/Stories/StoryList.json");
        JsonUtility.FromJsonOverwrite(jsonString, sli);
        return sli.story;
    }
    public string[] LoadHiddenStoryList()
    {
        //loads a hiddenstory list array
        StoryList sli = new StoryList();

        string jsonString = File.ReadAllText(path + "/JSON/Stories/HiddenStoryList.json");
        JsonUtility.FromJsonOverwrite(jsonString, sli);
        return sli.story;
    }
    public string[] LoadSarray()
    {
        //loads storylist array
        dt.text = "loadscenearray start";
        StoryList sl = new StoryList();
        string jsonString = File.ReadAllText(path + "/JSON/Stories/StoryList.json");
        JsonUtility.FromJsonOverwrite(jsonString, sl);
        dt.text = "loadscenearray success";
        return sl.story;
    }
    public StoryList LoadResourcesStoryListClass(bool hidden)
    {
        //loads story list class from resources
        StoryList sl = new StoryList();
        string p = "JSON/Stories/StoryList.json";
        string p2 = p.Replace(".json", "");
        Debug.Log(p2);
        TextAsset t = Resources.Load<TextAsset>(p2);
        string jsonString = t.text;
        JsonUtility.FromJsonOverwrite(jsonString, sl);
        return sl;
    }
    public StoryList LoadStoryListClass(bool hidden)
    {
        //loads story list class from saves
        StoryList sl = new StoryList();
        string jsonString = "";
        if (hidden)
        {
            try
            {
                jsonString = File.ReadAllText(path + "/JSON/Stories/HiddenStoryList.json");
            }
            catch
            {
                sl = new StoryList();
            }
        }
        if (!hidden)
        {
            jsonString = File.ReadAllText(path + "/JSON/Stories/StoryList.json");
        }
        JsonUtility.FromJsonOverwrite(jsonString, sl);
        return sl;
    }
    public ScenarioClass LoadSaveFile(string storyname, string sceneName)
    {
        ScenarioClass sc = new ScenarioClass();
        string jsonString = File.ReadAllText(path + "/JSON/Saves/" + storyname + "/" + sceneName + ".json");
        JsonUtility.FromJsonOverwrite(jsonString, sc);
        return sc;
    }
    public ScenarioClass GetResourceScenario(string storyname, string sceneName)
    {
        //returns a scenarioclass from resources (Scene)
        ScenarioClass sc = new ScenarioClass();
        string p = "JSON/Stories/" + storyname + "/" + sceneName + ".json";
        Debug.Log(p);
        string p2 = p.Replace(".json", "");
        Debug.Log(p2);
        TextAsset t = Resources.Load<TextAsset>(p2);
        string jsonString = t.text;
        JsonUtility.FromJsonOverwrite(jsonString, sc);
        return sc;
    }
    public ScenarioClass GetScenario(string storyname, string sceneName)
    {
        //gets scenario from saves
        ScenarioClass sc = new ScenarioClass();
        string jsonString = File.ReadAllText(path + "/JSON/Stories/" + storyname + "/" + sceneName + ".json");
        JsonUtility.FromJsonOverwrite(jsonString, sc);
        return sc;
    }
    public string[] LoadSceneList(string story)
    {
        //gets scenelist from saves
        SceneArray sa = new SceneArray();
        string jsonString = File.ReadAllText(path + "/JSON/Stories/" + story + "/SceneList.json");
        JsonUtility.FromJsonOverwrite(jsonString, sa);
        return sa.scenes;
    }
    public List<string> LoadAllStoryLists()
    {
        //loads both hidden and basic story lists, returns as a list
        StoryList sl = LoadResourcesStoryListClass(false);
        StoryList sl2 = LoadResourcesStoryListClass(true);
        List<string> s = new List<string>() { };
        List<string> s1 = new List<string>() { };
        List<string> s2 = new List<string>() { };
        s1 = Debugsy.ArrayToList(sl.story);
        s2 = Debugsy.ArrayToList(sl2.story);
        s = Debugsy.CombineLists(s1, s2);
        return s;
    }
    public void SaveSaveFile(ScenarioClass sc, string storyName)
    {
        //save a savefile to json
        string json = JsonUtility.ToJson(sc);
        File.WriteAllText(path + "/JSON/Saves/" + storyName + "/" + sc.thisScenario + ".json", json);
    }
    public void SaveSceneToJson(ScenarioClass sc, string storyName)
    {
        //saves a scenarioclass to json
        bool b = false;
        string[] s = LoadSarray();
        SceneArray sa = new SceneArray();
        string[] scenes = LoadSceneList(storyName);
        string[] newscenes = new string[] { };

        for (int x = 0; x < scenes.Length; x++)
        {
            if (scenes[x] == sc.thisScenario)
            {
                b = true;
            }
            else
            {
                Debug.Log("not same");
            }
        }

       
        if(b)
        {
            newscenes = new string[scenes.Length];
            newscenes = scenes;
        }
        if(!b)
        {
            if (scenes.Length == 0)
            {
                newscenes = new string[1];
                newscenes[0] = sc.thisScenario;
            }
            else
            {            
                newscenes = new string[scenes.Length + 1];
                for(int xi = 0; xi < scenes.Length; xi++)
                {
                    newscenes[xi] = scenes[xi];
                }
                newscenes[newscenes.Length-1] = sc.thisScenario;
            }
        }
        
        sa.scenes = newscenes;
        string jst = JsonUtility.ToJson(sa);

        for (int i = 0; i <= s.Length; i++)
        {
            Debug.Log(storyName + " / " + s[i]);
            if (storyName == s[i])
            {
                dt.text = "savingscene";
                string jsonString = JsonUtility.ToJson(sc);
                File.WriteAllText(path + "/JSON/Stories/" + storyName + "/" + sc.thisScenario + ".json", jsonString);
                if (!hidden)
                {
                    File.WriteAllText(path + "/JSON/Stories/" + storyName + "/SceneList.json", jst);
                }
                if (hidden)
                {
                    File.WriteAllText(path + "/JSON/Stories/" + storyName + "/HiddenSceneList.json", jst);
                }
                break;
            }
            else
            {

            }
        }
       

        Directory.CreateDirectory(path + "/JSON/Stories/" + storyName);
        string jsonStringi = JsonUtility.ToJson(sc);
        File.WriteAllText(path + "/JSON/Stories/" + storyName + "/" + sc.thisScenario + ".json", jsonStringi);
        if (!hidden)
        {
            File.WriteAllText(path + "/JSON/Stories/" + storyName + "/SceneList.json", jst);
        }
        if (hidden)
        {
            File.WriteAllText(path + "/JSON/Stories/" + storyName + "/HiddenSceneList.json", jst);
        }
        dt.text = "scenesaved";
    }

    
}
public class Character
{
    //character class
    public string name;
    public int gender;
    public int[] gear;

    public Character(string s, int g, int[]ga)
    {
        name = s;
        gender = g;
        gear = ga; //ga = gear array
    }
    public Character() { }
}
public class StoryList
{
    //storylist class
    public string[] story;
    public bool[] hidden;
    public string[] password;

    public StoryList()
    {
        story = new string[0];
        hidden = new bool[0];
        password = new string[0];
    }
        

   
}
public class CharacterArray
{
    //character array class
    public string[] characters;
}
public class SceneArray
{
    //scene array class
    public string[] scenes;
}
public class HiddenStory
{
    //hidden story class
    string name;
    string description;
    bool hidden;
}
public class CharacterLista
{
    //character list scene containing List<>
    public List<string> characters;
}

